# frozen_string_literal: true

module Subtitles
  module String
    # https://stackoverflow.com/a/21369850
    def self.from(subs_path)
      font = FontPreset.get

      "subtitles=#{subs_path}:force_style='" \
      "FontName=#{font.name}," \
      "Fontsize=#{font.size}," \
      "Shadow=#{font.shadow}," \
      "Outline=#{font.outline}," \
      "PrimaryColour=&H#{font.alpha}#{font.fg}," \
      "OutlineColour=&H#{font.alpha}#{font.bg}," \
      "BackColour=&H#{font.alpha}#{font.bg}'"
    end
  end
end
