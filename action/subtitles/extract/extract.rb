# frozen_string_literal: true

require_relative "srt_file"
require_relative "internal/internal"
require_relative "external/external"

module Subtitles
  module Extract
    def self.subs_path(extractor)
      return extractor.srt_file.path if $opt.dry_run

      if File.zero? extractor.srt_file.path and not $opt.sub.edit
        throw :halt, Color.err["Empty subtitles."]
      end

      edit(extractor.srt_file.path) if $opt.sub.edit
      extractor.srt_file.path
    end

    private

    def self.edit(subs_path)
      system("editor", subs_path)
    end
  end
end
