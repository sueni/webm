# frozen_string_literal: true

module Subtitles
  module Extract
    module External
      class Cmd
        def initialize(**args)
          @args = args
          @args[:encoding] = "utf-8"
        end

        def call
          @extractor = Args.new(**@args)
          first_attempt or second_attempt or
            throw :halt, Color.err["Subtitles extraction failed."]

          Extract.subs_path(@extractor)
        end

        def first_attempt
          @extractor.call do
            system(*@extractor)
          end.success?
        end

        def second_attempt
          @args[:encoding] = "windows-1251"
          @extractor = Args.new(**@args)

          @extractor.call do
            system(*@extractor)
          end.success?
        end
      end
    end
  end
end
