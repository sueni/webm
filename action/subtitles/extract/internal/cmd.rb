# frozen_string_literal: true

module Subtitles
  module Extract
    module Internal
      class Cmd
        def initialize(**args)
          @args = args
        end

        def call
          extractor = Args.new(**@args)

          extractor.call { system(*extractor) }.success? or
            throw :halt, Color.err["Subtitles extraction failed."]

          Extract.subs_path(extractor)
        end
      end
    end
  end
end
