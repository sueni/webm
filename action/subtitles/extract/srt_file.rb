# frozen_string_literal: true

require "tempfile"

module Subtitles
  module Extract
    module SRTFile
      def srt_file
        @@srt_file ||= Tempfile.new(%w{subtitles_for_webm .srt})
      end
    end
  end
end
