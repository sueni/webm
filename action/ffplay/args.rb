# frozen_string_literal: true

module FFplay
  class Args < DryRun
    def initialize(**args)
      populate do |p|
        p.push(
          "ffplay", "-loglevel", "panic", "-ss", args[:start_position],
          "-i", args[:input_file], "-t", args[:duration], *_mute,
          *args[:video_filter], "-loop", "0"
        )
      end
    end

  private

    def _mute
      return unless $opt.mute
      %w[-an]
    end
  end
end
