# frozen_string_literal: true

module FFplay
  class Cmd
    def initialize(**args)
      @args = args
    end

    def call
      ffplay = Args.new(**@args)

      ffplay.call do
        Process.detach spawn(
          *ffplay, [:in, :out, :err] => "/dev/null",
        )
      end
    end
  end
end
