# frozen_string_literal: true

module SurroundingKeyframes
  SECONDS_PAD = 20

  class Cmd
    def initialize(input_file:, start_position:)
      @input_file = input_file
      @position = start_position
    end

    def call(mode)
      keyframe = Args.new(@input_file, intervals)

      keyframe.call(force: true) do
        IO.popen(keyframe.to_a, &:readlines)
          .select { |i| i.start_with?("pkt_pts_time") }
          .map { |k| k.split('=').last.to_f }
          .then { |s| BorderFloats.new(s, @position).public_send(mode) }
      end.success? or halt

      keyframe.output
    end

  private

    def halt
      throw :halt, Color.err["Receding keyframe detection failed."]
    end

    def intervals
      format(
        "%s%%%s",
        (@position - SECONDS_PAD).clamp(0, @position),
        @position + SECONDS_PAD
      )
    end
  end
end
