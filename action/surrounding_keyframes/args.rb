# frozen_string_literal: true

module SurroundingKeyframes
  class Args < DryRun
    def initialize(input_file, intervals)
      populate do |k|
        k.push(
          "ffprobe", "-i", input_file.to_arg, "-v", "quiet",
          "-read_intervals", intervals, "-select_streams", "v",
          "-skip_frame", "nokey", "-show_entries", "frame=pkt_pts_time",
        )
      end
    end
  end
end
