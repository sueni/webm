# frozen_string_literal: true

module DetectCrop
  class Cmd
    def initialize(**params)
      @params = params
      @samples = 3
      @detected_crops = []
    end

    def call
      sample_positions.each do |position|
        detector = Args.new(
          input_file: @params[:input_file],
          position: position,
        )

        area = Area.from_whxy(crop_read detector)
        detector.call(force: true) do
          area.to_xywh_spec or self.throw
        end.output and @detected_crops << Crop.new(area)
      end

      pick_crop or throw_error
    end

  private

    def throw_error
      throw :halt, Color.err["Failed to detect crop."]
    end

    def crop_read(detector)
      io = IO.popen(detector.to_a, :err => [:child, :out])
      io.read[/crop=(\d+:\d+:\d+:\d+)/, 1]
    ensure
      io.close
    end

    def pick_crop
      if (max_crop = @detected_crops.max)
        max_crop if max_crop < Crop.new(Area.from_whxy @params[:ffprobe].crop)
      end
    end

    def sample_positions
      @samples.times.map do |n|
        (@params[:start_position] + chunk_length * (n + 1)).round(3)
      end
    end

    def chunk_length
      (@params[:stop_position] - @params[:start_position]) / (@samples + 1)
    end
  end
end
