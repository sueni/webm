# frozen_string_literal: true

module DetectCrop
  class Args < DryRun
    def initialize(input_file:, position:)
      populate do |d|
        d.push(
          "ffmpeg", "-hide_banner", "-ss", position, "-i", input_file,
          "-vframes", "2", "-vf", "cropdetect=16:2:0",
          "-f", "null", "/dev/null"
        )
      end
    end

  private

    def method_name
      {hi: 16, med: 24, low: 32}.fetch($opt.detect_crop, :med)
    end
  end
end
