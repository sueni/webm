# frozen_string_literal: true

module Itemizable
  def iterate(items, message, color, bullet = "- ")
    return if items.empty?
    printf("%s:\n#{bullet}%s\n\n",
           message,
           items.map(&color.method(:call)).join("\n#{bullet}"))
  end
end
