# frozen_string_literal: true

module ClashChecker
  def self.call(pairs)
    pairs.flat_map do |opair|
      pairs.map do |ipair|
        if File.exist? opair.last
          next [opair, [opair.last, Color.err["<exists>"]]]
        end

        [opair, ipair] if opair.last == ipair.first
      end
    end.compact
  end
end
