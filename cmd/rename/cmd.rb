# frozen_string_literal: true

require_relative "file_ranger"
require_relative "clash_checker"
require_relative "itemizable"
require_relative "../../lib/role/colorizeable"

module Rename
  class Cmd
    using Colorizeable
    include Itemizable

    def initialize(input_files)
      input_files = input_files.apply(
        Rule.new(webm: 0.., mp4: 0..).limit_amount(1..)
      )
      @range = FileRanger.new(input_files)
    end

    def call
      check_for_names_collision @range.rename_pairs
      iterate @range.good_files, "Already good", Color.succ
      iterate @range.untitled, "Empty metatitle", Color.err
      @range.rename_pairs.each { |pair| rename(*pair) }
    end

  private

    def check_for_names_collision(pairs)
      return if (clashes = ClashChecker.call pairs).empty?

      puts Color.danger["Filenames clash:"]

      clashes.each do |clash|
        clash.each do |old_name, new_name|
          renaming_details(old_name, new_name)
        end
        puts
      end

      abort
    end

    def rename(old_name, new_name)
      renaming_details(old_name, new_name)
      File.rename old_name, new_name if $opt.real_run
    end

    def renaming_details(old_name, new_name)
      m1, m2 = markers

      printf("%s %s\n%s %s\n",
             m1,
             old_name.ls_color,
             m2,
             new_name.ls_color)
    end

    def markers
      %w[┌─● └─>].map { |m| $opt.real_run ? m : Color.warn[m] }
    end
  end
end
