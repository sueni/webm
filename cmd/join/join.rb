# frozen_string_literal: true

require "tempfile"
require "shellwords"
require_relative "files_list"

require_relative "args"
require_relative "cmd"
