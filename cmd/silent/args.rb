# frozen_string_literal: true

module Silent
  class Args < DryRun
    def initialize(
      input_file,
      _dummy_stereo_input,
      _silent_interval,
      out_file
    )
      populate do |s|
        s.push(
          "ffmpeg", "-y", "-loglevel", "error", "-nostdin",
          *_dummy_stereo_input, "-i", "file:#{input_file}",
          "-c:v", "copy", *_silent_interval, out_file
        )
      end
    end
  end
end
