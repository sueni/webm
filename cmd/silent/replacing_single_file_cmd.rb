# frozen_string_literal: true

module Silent
  class ReplacingSingleFileCmd < KeepingSingleFileCmd
    using PathExtensions

    def report_success
      with_real_success do
        out_file.summary("Silent")
        out_file.rename to: result
      end
    end

    def out_file
      @out_file ||= OutFile.new(
        @input_file.basename,
        ext: @input_file.ext,
        dir: "/tmp"
      )
    end

  private

    def _dummy_stereo_input
      %w[-f lavfi -i anullsrc=channel_layout=stereo]
    end

    def _silent_interval
      %w[-shortest -map 1:0]
    end

    def result
      return @input_file if same_dir?
      Pathname.pwd.join(@input_file.basename)
    end

    def same_dir?
      out_file.path.same_dir?(@input_file.path)
    end
  end
end
