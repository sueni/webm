# frozen_string_literal: true

module Silent
  class KeepingSingleFileCmd < Successor
    def initialize(input_file)
      @input_file = input_file
    end

    def call
      silent_args.call { system(*silent_args) }.success? or exit 1
      report_success
    end

    def out_file
      @out_file ||= OutFile.new(
        @input_file.name,
        "silent",
        ext: @input_file.ext
      )
    end

  private

    def report_success
      with_real_success { out_file.summary("Silent") }
    end

    def silent_args
      @silent_args ||= Args.new(
        @input_file,
        _dummy_stereo_input,
        _silent_interval,
        out_file.path
      )
    end

    def _dummy_stereo_input
    end

    def _silent_interval(fade_duration = 0.5)
      AudioFilter.new(
        format(
          "afade=enable='between(t,%1$g,%2$g)':t=out:st=%1$g:d=%3$g",
          (span.start_position.to_f - fade_duration),
          span.stop_position,
          fade_duration
        )
      )
    end

    def span
      @span ||= Span.new(
        Edges.new @input_file, @input_file.duration
      )
    end
  end
end
