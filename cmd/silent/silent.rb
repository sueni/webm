# frozen_string_literal: true

require_relative "keeping_single_file_cmd"
require_relative "../../extensions/path_extensions"
require_relative "replacing_single_file_cmd"

require_relative "single_file_cmd"
require_relative "../../lib/role/shared_input_files_iterator"
require_relative "cmd"

require_relative "args"

