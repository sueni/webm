# frozen_string_literal: true

module Silent
  class Cmd
    include SharedInputFilesIterator

    def initialize(input_files)
      @input_files = input_files.apply(
        Rule.new.limit_amount(1..)
      )

      @store_file_only = input_files.stored_file_only?
    end

  private

    def shared_data
      @store_file_only
    end
  end
end
