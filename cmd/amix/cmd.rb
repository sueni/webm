# frozen_string_literal: true

module Amix
  class Cmd < Successor
    def initialize(input_files)
      @file_sorter = FFprobe::Sorter.new(
        input_files.apply(
          Rule.new(
            flac: 0..1,
            mp3: 0..1,
            mp4: 0..1,
            opus: 0..1,
            webm: 0..2,
          ).limit_amount(2..2)
        )
      )

      arrange_files_by_type
      configure_meta_title
    end

    def call
      amix = Args.new(@video_file, @audio_file, out_file)
      amix.call do
        ProgressTracker.new(
          command: amix.to_a,
          duration: amix.duration,
          framerate: 1,
        ).call
      end.success? or exit 1
      with_real_success { out_file.summary("Amix") }
    rescue Interrupt
      out_file.unlink
      exit
    end

  private

    def arrange_files_by_type
      @video_file, @audio_file =
        @file_sorter.visual_and_audible
    end

    def configure_meta_title
      Metadata.set_from @video_file.title
      Metadata.audio = audio_title
    end

    def audio_title
      @audio_file.audio_meta or
        @audio_file.title
    end

    def out_file
      @out_file ||= OutFile.new(
        "amix",
        @video_file.id,
        @audio_file.id,
        ext: ".#{@video_file.container}",
      )
    end
  end
end
