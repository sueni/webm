# frozen_string_literal: true

module Amix
  class Args < DryRun
    def initialize(video_file, audio_file, out_file)
      @video_file = video_file
      @audio_file = audio_file
      @out_file = out_file

      populate do |m|
        m.push(
          "ffmpeg", "-y", "-loglevel", "error", "-stats",
          "-i", video_file.to_arg, *_audio_start_position,
          *_loop_audio_stream, "-i", audio_file.to_arg,
          *Metadata.to_arg, *_complex_filter, *_audio_bitrate,
          *_copy_video, *_duration, @out_file
        )
      end
    end

    def duration
      @video_file.duration
    end

  private

    def _loop_audio_stream
      loops = duration.fdiv(@audio_file.duration).ceil
      return if loops < 2
      ["-stream_loop", "#{loops}"]
    end

    def span_audio
      @span_audio ||= Span.new(
        Edges.new(
          @audio_file.path,
          @audio_file.duration
        )
      )
    end

    def _duration
      ['-t', duration]
    end

    def _complex_filter
      comp = ComplexFilter::Build.new do |cf|
        cf.add_map("0:v")

        cf.add_task(:vol1, "0:a", "a0", nomap: true) do |t|
          t << "volume=%s" % foreground_volume
        end

        cf.add_task(:vol2, "1:a", "a1", nomap: true) do |t|
          t << "volume=%s" % background_volume
        end

        cf.add_task(:amix, %w[a0 a1], "a") do |t|
          t << "amix=inputs=2"
          t << fade_audio
        end
      end
    end

    def foreground_volume
      $opt.volume or 1.0 + background_volume
    end

    def background_volume
      $opt.back_volume or 0.1
    end

    def fade_audio
      duration = if $opt.span.stop_position
                   $opt.span.stop_position
                 else
                   @video_file.duration
      end
      FadeAudio.new(0.5).duration(duration).to_arg
    end

    def _audio_start_position
      return if $opt.span.start_at_beginning
      ss = span_audio.start_position.to_f
      ss = ss_to_keep_the_end if $opt.span.stop_at_end
      ["-ss", ss] if ss > 0
    end

    def ss_to_keep_the_end
      [span_audio.seconds - @video_file.duration, 0].max
    end

    def _copy_video
      %w[-c:v copy]
    end

    def _audio_bitrate
      return if $opt.audio_bitrate.boring?
      $opt.audio_bitrate.to_a
    end
  end
end
