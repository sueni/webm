# frozen_string_literal: true

module Convert
  class Pass1Args < DryRun
    include PassShared

    def initialize(input_file, span, passlog)
      @span = span

      populate do |p1|
        p1.push(
          *_preamble, *_start_position, "-i", input_file,
          *_seconds, "-an", "-pass", "1", "-passlogfile",
          passlog.internal, "-f", "webm", "/dev/null"
        )
      end
    end
  end
end
