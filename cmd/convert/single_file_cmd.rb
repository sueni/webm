# frozen_string_literal: true

module Convert
  class SingleFileCmd < Successor
    attr_writer :shared_data

    def self.input_file
      @@input_file
    end

    def initialize(input_file)
      @input_file = input_file
      @@input_file = input_file
      configure_meta_title
    end

    def call
      AudioStreamSelector.call(@input_file.ffprobe)

      info.display unless $opt.dry_run
      preview if $opt.preview
      unless $opt.dry_run
        return if $opt.info_only or $opt.preview
      end

      run1
      run2 or emergency_exit

      with_real_success do
        out_file.summary("Convert", show_metadata: false)
      end
    end

  private

    def emergency_exit
      out_file.unlink
      exit 1
    end

    def configure_meta_title
      Metadata.video = @input_file.title
      Metadata.filepath = @input_file
    end

    def run1
      return true if passlog.pass2.reusable?

      pass1.call do
        ProgressTracker.new(
          command: pass1.to_a,
          duration: span.result_seconds,
          framerate: @input_file.framerate,
          color: Color.casual,
          clean_up: false
        ).call
      end.success? and (passlog.transit unless $opt.dry_run)
    rescue Interrupt
      Console.clean_line
      emergency_exit
    end

    def run2
      pass2.call do
        ProgressTracker.new(
          command: pass2.to_a,
          duration: span.result_seconds,
          framerate: @input_file.framerate,
          extra: out_file.filesize,
        ).call

        passlog.rotate
      end.success?
    rescue Interrupt
      Console.clean_line
      emergency_exit
    end

    def pass1
      video_filter
      Pass1Args.new(@input_file, span, passlog.pass1)
    end

    def pass2
      @pass2 ||= Pass2Args.new(
        input_file: @input_file,
        span: span,
        passlog: passlog.pass2,
        video_filter: video_filter,
        video_bitrate: bitsize.video_bitrate,
        out_file: out_file.name,
      )
    end

    def span
      @span ||= Span.new(
        Edges.new(
          @input_file,
          @input_file.duration
        )
      )
    end

    def passlog
      @passlog ||= PassLog.new(out_file.passlog_name.to_s)
    end

    def video_filter
      @video_filter ||= VideoFilterBuilder.new(
        input_file: @input_file,
        restrained_dimensions: restrained_dimensions,
        span: span,
        crop: crop,
        pixelize: pixelize,
        overlay_manager: overlay_manager
      ).call
    end

    def out_file
      @out_file ||= WEBMFile.new(
        @input_file.id,
        span.start_position.id,
        span.stop_position.id
      )
    end

    def bitsize
      @bitsize ||= BitrateFilesize.new(
        video_dimensions: cropped_dimensions,
        seconds: span.result_seconds,
        audio_bitrate: $opt.audio_bitrate.value,
        bitrate_factor: overlay_manager.bitrate_factor,
      )
    end

    def overlay_manager
      @overlay_manager ||= OverlayManager.new(
        input_file: @input_file,
        position: $opt.overlay,
        crop: PositionsStore.crop
      ).tap do |manager|
        manager.configure_bitrate_factor(
          pixelfactor: pixelize.bitrate_factor,
          blur: $opt.blur.value
        )
      end
    end

    def cropped_dimensions
      @cropped_dimensions ||=
        ($opt.rotate ? RotatedCroppedDimensions : CroppedDimensions).new(
        original_dimensions: @input_file.ffprobe,
        restrained_dimensions: restrained_dimensions,
        crop: crop,
      )
    end

    def restrained_dimensions
      @restrained_dimensions ||= RestrainedDimensions.new(
        width: RestrainedWidth.new(
          original: @input_file.width,
          explicit_limit: $opt.width,
          implicit_limit: 1280,
          crop_limit: crop.width
        ),
        height: RestrainedHeight.new(
          original: @input_file.height,
          explicit_limit: $opt.height,
          implicit_limit: 1280,
          crop_limit: crop.height
        )
      )
    end

    def pixelize
      @pixelize ||= Pixelize.new(
        $opt.pixels_per_side,
        cropped_dimensions.minimal_side_length
      )
    end

    def info
      Inform.new(
        duration: Position.from_float(span.result_seconds).to_s(true),
        target_file_size: bitsize.target_file_size_kbites,
        video_bitrate: bitsize.video_bitrate,
        video_dimensions: cropped_dimensions,
        pixelize: pixelize,
      )
    end

    def preview
      FFplay::Cmd.new(
        input_file: @input_file,
        start_position: span.start_position,
        duration: span.seconds,
        video_filter: video_filter,
      ).call
      exit
    end

    def crop
      @crop ||=
        case
        when $opt.crop.active?
          Crop.new(
            Area.from_xywh(
              $opt.crop.value ? $opt.crop.value : PositionsStore.crop
            )
          )
        when $opt.detect_crop
          DetectCrop::Cmd.new(
            input_file: @input_file,
            ffprobe: @input_file.ffprobe,
            start_position: span.start_position.to_f,
            stop_position: span.stop_position.to_f,
          ).call
        else
          Struct.new(:width, :height, :x_offset, :y_offset).new
        end
    end
  end
end
