# frozen_string_literal: true

module Convert
  class Pass2Args < DryRun
    include PassShared
    include Streamable

    def initialize(
      input_file:, span:, passlog:,
      video_filter:, video_bitrate:, out_file:
    )
      @input_file = input_file
      @span = span
      @video_bitrate = video_bitrate

      populate do |p2|
        p2.push(
          *_preamble, *_start_position, "-i", input_file,
          *_logo_image, *_seconds, *_map_streams, *_restrict_stereo,
          *Metadata.to_arg, *$opt.audio_bitrate.to_a, *_audio_filter,
          *_frame_rate, *_video_bitrate, *video_filter, "-aq-mode",
          "2", "-qcomp", "1.0", "-threads", "4", "-pass", "2",
          "-passlogfile", passlog.internal, "-f", "webm", out_file
        )
      end
    end

  private

    def _logo_image
      ["-i", $opt.logo.content] if $opt.logo.image?
    end

    def _map_streams
      if $opt.map_streams and not $opt.mute
        return $opt.map_streams.split(",")
          .flat_map { |s| ["-map", zero_stream(s)] }
      end

      ["-map", "0:v:0", *($opt.mute ? nil : ["-map", "0:a:0?"])]
    end

    def _restrict_stereo
      return if $opt.mute
      %w[-ac 2]
    end

    def _audio_filter
      return if $opt.mute
      DisableableAudioFilter.new do |af|
        af << "volume=#{$opt.volume}" if $opt.volume
        af << fade_audio if $opt.fade_audio
        af << audio_speed if $opt.speed.active?
        af.disable if $opt.speed.active? and 1.fdiv($opt.speed.value) < 0.5
      end
    end

    def audio_speed
      "atempo=#{1.fdiv $opt.speed.value}"
    end

    def fade_audio
      $opt.fade_audio.duration(@span.seconds).to_arg
    end

    def _frame_rate
      return unless $opt.frame_rate
      ["-r", $opt.frame_rate]
    end

    def _video_bitrate
      ["-b:v", "#{@video_bitrate}k"]
    end
  end
end
