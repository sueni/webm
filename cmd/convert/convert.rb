# frozen_string_literal: true

require_relative "inform"
require_relative "pass_shared"
require_relative "pass1_args"

require_relative "../../lib/role/streamable"
require_relative "pass2_args"

require_relative "../../lib/overlay/overlay_manager"
require_relative "../../lib/filter/video_filter_builder"
require_relative "../../lib/audio_stream_selector"
require_relative "../../lib/bitrate_filesize"
require_relative "../../lib/video_dimensions/rotated_cropped_dimensions"
require_relative "../../lib/video_dimensions/restrained_dimensions"
require_relative "../../lib/ui/console"
require_relative "../../action/ffplay/ffplay"
require_relative "single_file_cmd"
require_relative "../../lib/role/shared_input_files_iterator"
require_relative "cmd"

require_relative "pass_log"
