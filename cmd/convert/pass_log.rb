# frozen_string_literal: true

module Convert
  class PassLog
    module Shared
      DIR = "/tmp"

      def internal
        @prefix + @basename
      end

      def external
        "#{internal}-0.log"
      end
    end

    class Pass1
      include Shared

      def initialize(basename)
        @basename = basename
        @prefix = "#{DIR}/pass1-"
      end
    end

    class Pass2
      include Shared

      def initialize(basename)
        @basename = basename
        @prefix = "#{DIR}/pass2-"
      end

      def reusable?
        File.size?(external)
      end
    end

    attr_reader :pass1, :pass2

    def initialize(basename)
      @pass1 = Pass1.new(basename)
      @pass2 = Pass2.new(basename)
    end

    def transit
      File.rename(pass1.external, pass2.external)
    end

    def rotate
      Dir["#{Shared::DIR}/*-0.log"].each do |log|
        File.unlink(log) unless log == pass2.external
      end
    end
  end
end
