# frozen_string_literal: true

module Convert
  module PassShared
    def _preamble
      %w[ffmpeg -y -loglevel error -stats -nostdin]
    end

    def _start_position
      return if $opt.span.start_at_beginning
      ["-ss", @span.start_position]
    end

    def _seconds
      return if $opt.span.start_at_beginning
      ["-t", @span.seconds * $opt.speed.value]
    end
  end
end
