# frozen_string_literal: true

module Frame
  module CommonFrame
    def args
      ["ffmpeg", "-y", "-loglevel", "error", *_unmute,
      "-framerate", "1", "-i", "file:#{@image}",
      *VideoFilterBuilder.to_arg, "-shortest", @out_file]
    end
  end
end
