# frozen_string_literal: true

require_relative "blink_frame"
require_relative "common_frame"
require_relative "video_filter_builder"
require_relative "args"

require_relative "single_file_cmd"
require_relative "../../lib/role/input_files_iterator"
require_relative "cmd"
