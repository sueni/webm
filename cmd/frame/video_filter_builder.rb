# frozen_string_literal: true

module VideoFilterBuilder
  extend self

  def to_arg
    VideoFilter.new do |vf|
      if $opt.width || $opt.height
        vf << scale
        vf << sharp
      end
    end
  end

private

  def scale
    format("scale=%s:%s,setsar=1:1", $opt.width || -1, $opt.height || -1)
  end

  def sharp
    "unsharp=5:5:0.5:5:5:0.0"
  end
end
