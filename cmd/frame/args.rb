# frozen_string_literal: true

module Frame
  class Args < DryRun
    include ($opt.blink_frame ? BlinkFrame : CommonFrame)

    def initialize(image, out_file)
      @image = image
      @out_file = out_file

      populate { |f| f.concat(args) }
    end

  private

    def _unmute
      return if $opt.mute
      # https://stackoverflow.com/a/12375018
      %w[-f lavfi -i anullsrc=channel_layout=stereo]
    end
  end
end
