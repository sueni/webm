# frozen_string_literal: true

module Frame
  class Cmd
    include InputFilesIterator

    def initialize(input_files)
      @input_files = input_files.apply(
        Rule.new(
          jpg: (0..),
          png: (0..)
        ).limit_amount(1..)
      )
    end
  end
end
