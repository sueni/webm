# frozen_string_literal: true

module Info
  class Cmd
    include InputFilesIterator

    def initialize(input_files)
      @input_files = input_files.apply(
        Rule.new(webm: 0.., mp4: 0..).limit_amount(1..)
      )
    end
  end
end
