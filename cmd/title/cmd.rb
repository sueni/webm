# frozen_string_literal: true

module Title
  class Cmd
    include SharedInputFilesIterator

    def initialize(input_files)
      @input_files = input_files.apply(
        Rule.new.limit_amount(1..)
      )
    end

  private

    def shared_data
      proc { @input_files.map(&:title) }
    end
  end
end
