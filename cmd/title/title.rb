# frozen_string_literal: true

require_relative "../../lib/out_file/file_summary"
require_relative "single_file_cmd"
require_relative "../../lib/role/shared_input_files_iterator"

require_relative "args"
require_relative "cmd"
