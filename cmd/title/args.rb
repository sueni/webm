# frozen_string_literal: true

module Title
  class Args < DryRun
    def initialize(input_file, temp_file)
      populate do |t|
        t.push(
          "ffmpeg", "-y", "-loglevel", "error", "-nostdin",
          "-i", input_file, *Metadata.to_arg!,
          "-codec", "copy", temp_file
        )
      end
    end
  end
end
