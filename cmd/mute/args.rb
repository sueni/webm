# frozen_string_literal: true

module Mute
  class Args < DryRun
    def initialize(input_file, out_file)
      populate do |m|
        m.push(
          "ffmpeg", "-y", "-loglevel", "error", "-nostdin",
          "-i", input_file, *Metadata.to_arg,
          "-c:v", "copy", "-an", out_file
        )
      end
    end
  end
end
