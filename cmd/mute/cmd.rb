# frozen_string_literal: true

module Mute
  class Cmd
    include InputFilesIterator

    def initialize(input_files)
      @input_files = input_files.apply(
        Rule.new.limit_amount(1..)
      )
    end
  end
end
