# frozen_string_literal: true

module Audio
  module Evoke
    class Cmd < Successor
      def initialize(input_file, span)
        @input_file = input_file
        @span = span
      end

      def call
        evoke = Args.new(@input_file, @span, out_file)
        evoke.call { system(*evoke) }.success? or exit 1
        with_real_success do
          out_file.summary("Evoke audio", show_metadata: false)
        end
        self
      end

      def out_file
        @out_file ||= OutFilePicker.call(
          File.extname(@input_file).then { |f| [f] }
        ).new(
          @input_file.id,
          @span.start_position.id,
          @span.stop_position.id
        )
      end
    end
  end
end
