# frozen_string_literal: true

require_relative "../../../lib/role/streamable"
require_relative "args"
require_relative "cmd"
