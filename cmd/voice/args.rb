# frozen_string_literal: true

module Voice
  class Args < DryRun
    def initialize(video_file, audio_file, out_file)
      @video_file = video_file
      @audio_file = audio_file
      @out_file = out_file

      warn_on_video_cut

      populate do |m|
        m.push(
          "ffmpeg", "-y", "-loglevel", "error", "-stats",
          *_static_image, "-i", video_file.to_arg,
          *_audio_start_position, "-i", audio_file.to_arg,
          *Metadata.to_arg, *_complex_filter, *_audio_bitrate,
          *_copy_video, *_copy_audio, *_downrate_hi_res_audio,
          *_duration, *_crf, @out_file
        )
      end
    end

    def duration
      return span_audio.seconds if @video_file.is_picture?
      return [span_audio.seconds,
              @video_file.duration].max if $opt.long
      [span_audio.seconds, @video_file.duration].min
    end

  private

    def warn_on_video_cut
      if @video_file.duration > duration
        warn Color.warn["Video is longer and will be cut"]
      end
    end

    def span_audio
      @span_audio ||= Span.new(
        Edges.new(
          @audio_file.path,
          @audio_file.duration
        )
      )
    end

    def _duration
      ['-t', duration]
    end

    def _complex_filter
      ComplexFilterBuilder.new(
        video_file: @video_file,
        span_audio: span_audio,
        duration: duration,
        options: $opt
      )
    end

    def _audio_start_position
      AudioStartPosition.pick_method(
        @video_file.is_picture?
      ).new(
        span_audio,
        @video_file,
      ).to_arg
    end

    def _copy_audio
      return if mutate_audio? or unsuitable_audio?
      %w[-c:a copy]
    end

    def unsuitable_audio?
      !@video_file.container_fit?(
        @audio_file.audio_codec)
    end

    def _copy_video
      %w[-c:v copy] unless @video_file.is_picture?
    end

    def _audio_bitrate
      return if $opt.audio_bitrate.boring?
      $opt.audio_bitrate.to_a
    end

    def mutate_audio?
      $opt.volume ||
        $opt.fade_audio ||
        $opt.audio_bitrate.active?
    end

    def _static_image
      return unless @video_file.is_picture?
      %w[-loop 1 -framerate 1]
    end

    def _crf
      return unless @video_file.is_picture?
      %w[-crf 35 -b:v 0]
    end

    def _downrate_hi_res_audio
      return unless @video_file.webm?
      return unless @audio_file.hi_res_audio?
      %w[-sample_fmt s16]
    end
  end
end
