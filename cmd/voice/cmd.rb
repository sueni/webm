# frozen_string_literal: true

module Voice
  class Cmd < Successor
    def initialize(input_files)
      @file_sorter = FFprobe::Sorter.new(
        input_files.apply(
          Rule.new(
            avi: 0..1,
            flac: 0..1,
            jpeg: 0..1,
            jpg: 0..1,
            m4a: 0..1,
            mov: 0..1,
            mp3: 0..1,
            mp4: 0..2,
            opus: 0..1,
            png: 0..1,
            webm: 0..2,
            webp: 0..1,
          ).limit_amount(2..2)
        )
      )

      arrange_files_by_type
      configure_meta_title
    end

    def call
      voice = Args.new(@video_file, @audio_file, out_file)
      voice.call do
        ProgressTracker.new(
          command: voice.to_a,
          duration: voice.duration,
          framerate: framerate,
        ).call
      end.success? or exit 1
      with_real_success { out_file.summary("Voice") }
    rescue Interrupt
      out_file.unlink
      exit
    end

  private

    def framerate
      $opt.zoom ? 25 : 1
    end

    def arrange_files_by_type
      @video_file, @audio_file =
        @file_sorter.visual_and_audible
    end

    def configure_meta_title
      Metadata.set_from @video_file.title
      Metadata.audio = audio_title if $opt.meta.set_audio
    end

    def audio_title
      @audio_file.audio_meta or
        @audio_file.title
    end

    def out_file
      @out_file ||= OutFile.new(
        "voice",
        @video_file.id,
        @audio_file.id,
        ext: ".#{output_container}",
      )
    end

    def output_container
      ContainerSelector.new(
        @video_file.container,
        @audio_file.container
      ).container
    end
  end
end
