# frozen_string_literal: true

module AudioStartPosition
  class ForPicture
    def initialize(audio_span, *)
      @audio_span = audio_span
    end

    def to_arg
      ["-ss", start_position]
    end

  private

    def start_position
      return 0 if start_at_beginning?
      @audio_span.start_position.to_f
    end

    def start_at_beginning?
      $opt.span.start_at_beginning ||
        implicit_audio_start_position?
    end

    def implicit_audio_start_position?
      @audio_span.start_position.to_f.zero?
    end
  end
end
