# frozen_string_literal: true

require_relative "for_picture"
require_relative "for_video"

module AudioStartPosition
  def self.pick_method(for_picture)
    for_picture ? ForPicture : ForVideo
  end
end
