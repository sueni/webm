# frozen_string_literal: true

class ContainerSelector
  MULTICONTAINERS = %i[webm mp4]

  def initialize(video_container, audio_container)
    @video_container = video_container
    @audio_container = audio_container
  end

  def container
    video_container || audio_container || :webm
  end

private

  def video_container
    @video_container if MULTICONTAINERS.include? @video_container
  end

  def audio_container
    @audio_container if MULTICONTAINERS.include? @audio_container
  end
end
