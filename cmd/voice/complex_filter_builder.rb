# frozen_string_literal: true

class ComplexFilterBuilder
  def initialize(video_file:, span_audio:, duration:, options:)
    @video_file = video_file
    @span_audio = span_audio
    @duration = duration
    @opt = options
    @zoom_speed = @span_audio.seconds.fdiv 180_000
    @framerate = 25
  end

  def to_a
    cf = ComplexFilter::Build.new

    cf.add_task(:video, "0:v", "v") do |v|
      v << zoompan_image if @opt.zoom
      v << scale_image if @opt.width || @opt.height
      v << unsharp if @opt.width || @opt.height
    end

    cf.add_task(:audio, "1:a", "a") do |a|
      a << "volume=#{@opt.volume}" if @opt.volume
      a << fade_audio if @opt.fade_audio
    end

    cf.to_a
  end

private

  def zoompan_image
    return unless @video_file.is_picture?

    Zoompan.new(
      spot: @opt.zoom,
      speed: @zoom_speed,
      duration: (@duration * @framerate),
      width: @video_file.width,
      height: @video_file.height
    ).to_s
  end

  def unsharp
    return unless @video_file.is_picture?
    "unsharp=5:5:0.5:5:5:0.0"
  end

  def scale_image
    return unless @video_file.is_picture?

    format(
      "scale=%s:%s,setsar=1:1",
      @opt.width || -1,
      @opt.height || -1
    )
  end

  def fade_audio
    @opt.fade_audio.duration(faded_audio_duration).to_arg
  end

  def faded_audio_duration
    if @video_file.is_picture? or @opt.long
      return @span_audio.seconds
    end

    [@video_file.duration, @span_audio.seconds].min
  end
end
