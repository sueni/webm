# frozen_string_literal: true

$LOAD_PATH.prepend __dir__

require_relative "lib/input_files/input_files"
require_relative "lib/options/cli"
require_relative "lib/ffprobe/ffprobe"
require_relative "lib/ui/positions_store"

autoload :Area, "lib/area"
autoload :Edges, "lib/span/edges"
autoload :Position, "lib/span/position"
autoload :Span, "lib/span/span"

autoload :Crop, "lib/crop"
autoload :DryRun, "lib/dry_run"
autoload :FadeAudio, "lib/fade_audio"
autoload :Pixelize, "lib/pixelize"
autoload :Successor, "lib/successor"
autoload :Metadata, "lib/metadata/metadata"
autoload :ProgressTracker, "lib/progress/progress_tracker"
autoload :Color, "lib/ui/color"
autoload :BorderFloats, "lib/border_floats"

autoload :OutFile, "lib/out_file/out_file"
autoload :FLACFile, "lib/out_file/flac_file"
autoload :MP4File, "lib/out_file/mp4_file"
autoload :OPUSFile, "lib/out_file/opus_file"
autoload :OutFilePicker, "lib/out_file/out_file_picker"
autoload :WEBMFile, "lib/out_file/webm_file"
autoload :WMVFile, "lib/out_file/wmv_file"

autoload :AudioFilter, "lib/filter/audio_filter"
autoload :DisableableAudioFilter, "lib/filter/disableable_audio_filter.rb"
autoload :VideoFilter, "lib/filter/video_filter"
autoload :ComplexFilter, "lib/filter/complex_filter"

autoload :DetectCrop, "action/detect_crop/detect_crop"
autoload :Subtitles, "action/subtitles/subtitles"
autoload :SurroundingKeyframes, "action/surrounding_keyframes/surrounding_keyframes"
