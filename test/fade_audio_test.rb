# frozen_string_literal: true

require "minitest/autorun"
require_relative "../lib/fade_audio"

describe FadeAudio do
  describe "integer as the agrument state" do
    before { @fa = FadeAudio.new(1).duration(5).to_arg }

    it "should return default string" do
      _(@fa).must_equal "afade=t=in:ss=0:d=1,afade=t=out:st=4:d=1"
    end
  end

  describe "floating as the agrument state" do
    before { @fa = FadeAudio.new(0.5).duration(10).to_arg }

    it "should return default string" do
      _(@fa).must_equal "afade=t=in:ss=0:d=0.5,afade=t=out:st=9.5:d=0.5"
    end
  end

  describe "length not convertible to Float" do
    it "must raise" do
      _(proc { FadeAudio.new("in=o.7") }).must_throw :halt
    end
  end

  describe 'if only "in" argument provided' do
    before { @fa = FadeAudio.new("in=0.7").duration(10).to_arg }

    it "should return only in-part" do
      _(@fa).must_equal "afade=t=in:ss=0:d=0.7"
    end
  end

  describe "if one of arguments is zero" do
    before { @fa = FadeAudio.new("in=0:out=0.5").duration(10).to_arg }

    it "should not be included into the filter" do
      _(@fa).must_equal "afade=t=out:st=9.5:d=0.5"
    end
  end

  describe 'if only "out" argument provided' do
    before { @fa = FadeAudio.new("out=0.4").duration(10).to_arg }

    it "should return only out-part" do
      _(@fa).must_equal "afade=t=out:st=9.6:d=0.4"
    end
  end

  describe 'if both "in" and "out" arguments provided' do
    before { @fa = FadeAudio.new("in=1:out=2").duration(10).to_arg }

    it "should return only both parts" do
      _(@fa).must_equal "afade=t=in:ss=0:d=1,afade=t=out:st=8:d=2"
    end
  end
end
