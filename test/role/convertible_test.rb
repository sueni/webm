# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/role/color_converter"

Converter = Class.new
Converter.include ColorConverter

describe Converter do
  before { @convertor = Converter.new }

  describe :rgb_to_bgr do
    it "should work" do
      _(@convertor.rgb_to_bgr("rrggbb")).must_equal "bbggrr"
    end
  end
end
