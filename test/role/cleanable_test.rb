# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/role/cleanable"

using Cleanable

describe Cleanable do
  describe :clean do
    it "empty input should return empty string" do
      _("".clean).must_equal ""
    end

    it 'should remove prefix ":file' do
      _("file:some title".clean).must_equal "some title"
    end

    it "should clean the title" do
      _("The.Silence.of.the.Lambs.1991.REMASTERED.BluRay.H264.AAC-RARBG"
        .clean).must_equal "The.Silence.of.the.Lambs.1991"
    end

    it "should clean the title" do
      _("david.wants.to.fly.(2010).dvdrip.x264.ac3.mkv"
        .clean).must_equal "david.wants.to.fly.2010"
    end

    it "should keep the opening bracker if there is a closing one" do
      string = "Loreena McKennitt - [The Wind That Shakes The Barley]"
      _(string.clean).must_equal string
    end

    it "should work" do
      _("Vinyl.S01E06.1080p.HDTV.DD5.1.H264-CasStudio"
        .clean).must_equal "Vinyl.S01E06"
    end

    it "should work" do
      _("RARBG.COM - FooBar.14.09.23.Baz-Quxx.XXX.720p.MP4-KTR"
        .clean).must_equal "[FooBar] - Baz-Quxx"
    end
  end
end
