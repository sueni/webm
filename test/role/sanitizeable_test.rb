# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/role/sanitizeable"

using Sanitizeable

describe Sanitizeable do
  describe :sanitize do
    it "should work" do
      _("foo, bar".sanitize).must_equal "foo_bar"
    end
  end

  describe :desanitize do
    it "should work" do
      _("foo_bar".desanitize).must_equal "foo bar"
    end
  end
end
