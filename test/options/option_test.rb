# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/options/cli"

SomeOption = Class.new(CLI::Option)

describe CLI::Option do
  before { @option = SomeOption.new 77 }

  describe "initial state" do
    it "value should be passive" do
      _(@option.value).must_equal 77
    end

    it "should not be active" do
      refute @option.active?
    end

    it "passive? and active? should be opposite" do
      refute @option.passive? == @option.active?
    end
  end

  describe "actively set state" do
    before { @option.set 88 }

    it "should update the value with active value" do
      _(@option.value).must_equal 88
    end

    it "should set the active type" do
      assert @option.active?
    end

    it "passive? and active? should be opposite" do
      refute @option.active? == @option.passive?
    end
  end
end
