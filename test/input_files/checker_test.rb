# frozen_string_literal: true
# vim:ft=ruby:fdm=marker:

require "minitest/autorun"
require_relative "../../lib/input_files/rule"
require_relative "../../lib/input_files/checker"

describe InputFiles::Checker do
  before { @checker = InputFiles::Checker }

  describe :amount do # {{{1
    before { @files = %w[foo.jpg bar.webm] }

    describe "insufficient amount" do # {{{2
      before { @rule = Rule.new.limit_amount(3..3) }

      it "should throw" do
        _(proc { @checker.new(@rule).verify(@files) })
          .must_throw(
            :halt,
            "Wrong input files quantity: 2 for 3..3"
          )
      end
    end # }}}2

    describe "expected amount" do # {{{2
      before { @rule = Rule.new.limit_amount(2..2) }

      it "should pass" do
        _(proc { @checker.new(@rule).verify(@files) })
          .must_be_silent
      end
    end # }}}2

    describe "overwhelming amount" do # {{{2
      before { @rule = Rule.new.limit_amount(1..1) }

      it "should throw" do
        _(proc { @checker.new(@rule).verify(@files) })
          .must_throw(
            :halt,
            "Wrong input files quantity: 2 for 1..1"
          )
      end
    end # }}}2

  end # }}}1

  describe :filter do # {{{1

    describe "type" do # {{{2

      describe "insufficient" do # {{{3
        before do
          @files = %w[foo.jpg]
          @rule = Rule.new(jpg: 1..1, webm: 1..1)
        end

        it "should throw" do
          _(proc { @checker.new(@rule).verify(@files) })
            .must_throw(
              :halt,
              "No files of mandatory type: webm"
            )
        end
      end # }}}3

      describe "fitting" do # {{{3
        before do
          @files = %w[foo.jpg bar.webm baz.webm]
          @rule = Rule.new(jpg: 1..3, webm: 2..3)
        end

        it "should pass" do
          _(proc { @checker.new(@rule).verify(@files) })
            .must_be_silent
        end
      end # }}}3

      describe "overwhelming" do # {{{3
        before do
          @files = %w[foo.jpg bar.opus]
          @rule = Rule.new(jpg: 1..1)
        end

        it "should throw" do
          _(proc { @checker.new(@rule).verify(@files) })
            .must_throw(
              :halt,
              "Provided unexpected type: opus"
            )
        end
      end # }}}3

      describe "optional" do # {{{3
        before do
          @files = %w[foo.jpg bar.webm]
          @rule = Rule.new(jpg: 1..1, webm: 1..1, opus: 0..1)
        end

        it "should pass" do
          _(@checker.new(@rule).verify(@files))
            .must_equal(@files.map { |f| InputFile.new f })
        end
      end # }}}3

    end # }}}2

    describe "quantity" do # {{{2

      describe "insufficient" do # {{{3
        before do
          @files = %w[foo.jpg]
          @rule = Rule.new(jpg: 2..2)
        end

        it "should throw" do
          _(proc { @checker.new(@rule).verify(@files) })
            .must_throw(
              :halt,
              "Type 'jpg' is out of boundaries: 1 for 2..2"
            )
        end
      end # }}}3

      describe "fitting" do # {{{3
        before do
          @files = %w[foo.jpg bar.webm baz.webm]
          @rule = Rule.new(jpg: 1..3, webm: 2..3)
        end

        it "should pass" do
          _(proc { @checker.new(@rule).verify(@files) })
            .must_be_silent
        end
      end # }}}3

      describe "overwhelming" do # {{{3
        before do
          @files = %w[foo.jpg bar.webm baz.webm]
          @rule = Rule.new(jpg: 1..1, webm: 0..1)
        end

        it "should throw" do
          _(proc { @checker.new(@rule).verify(@files) })
            .must_throw(
              :halt,
              "Type 'webm' is out of boundaries: 2 for 0..1"
            )
        end
      end # }}}3

    end # }}}2

  end # }}}1

  describe "case insensitivity" do # {{{1
    before do
      @files = %w[foo.jPg bar.wEBm]
      @rule = Rule.new(jpg: 1..1, webm: 1..1)
    end

    it "should be case insensitive" do
      _(@checker.new(@rule).verify(@files)).must_equal(
        @files.map { |f| InputFile.new f }
      )
    end
  end # }}}1

  describe "unpacking the only file" do # {{{1

    describe "when the filter implies it" do # {{{2
      before do
        @files = %w[foo.jpg]
        @rule = Rule.new(jpg: 1..1)
      end

      it "should pass" do
        _(@checker.new(@rule).verify(@files)).must_be_instance_of InputFile
      end
    end # }}}2

    describe "when the filter doesn't imply it" do # {{{2
      before do
        @files = %w[foo.jpg]
        @rule = Rule.new(jpg: 1..2)
      end

      it "should pass" do
        _(@checker.new(@rule).verify(@files)).must_be_instance_of Array
      end
    end # }}}2

  end # }}}1

end
