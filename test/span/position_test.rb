# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/span/position"

describe Position do
  describe :from_float do
    before { @result = "00:00:10.999000" }

    it "should raise on input nil" do
      _(proc { Position.from_float nil }).must_raise TypeError
    end

    it "should raise on negative value" do
      _(proc { Position.from_float(-1) }).must_raise ArgumentError
    end

    it "should work with Int" do
      position = Position.from_float(10.999)
      _(position.to_s).must_equal @result
    end

    it "should work with Float" do
      position = Position.from_float(10.999)
      _(position.to_s).must_equal @result
    end

    it "should fail with String" do
      _(proc { Position.from_float("10") }).must_raise
    end
  end

  describe :new do
    it "should raise if the argument is not a Time" do
      _(proc { Position.new(10) }).must_raise
    end
  end

  describe :to_s do
    it "should remove meaningless zeros" do
      position = Position.from_float(10.555)
      _(position.to_s(true)).must_equal "00:10"
    end
  end

  describe :<=> do
    before do
      @p1 = Position.new(Time.now)
      @p2 = Position.new(Time.now + 10)
      @p3 = Time.now + 20
    end

    it "p2 should be greater that p1" do
      assert(@p2 > @p1)
    end

    it "should raise" do
      _(proc { @p3 > @p2 }).must_raise TypeError
    end
  end

  describe :- do
    before do
      now = Time.now
      @p1 = Position.new(now)
      @p2 = Position.new(now + 10.123)
      @p3 = Time.now + 20
    end

    it "should substruct" do
      _(@p2 - @p1).must_equal 10.123
    end

    it "should raise" do
      _(proc { @p3 - @p2 }).must_raise TypeError
    end
  end
end
