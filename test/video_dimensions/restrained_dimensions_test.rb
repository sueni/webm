# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/video_dimensions/restrained_dimensions"

describe RestrainedDimensions do
  describe "implicitly restrained landscape video" do
    before do
      @restrained_width = RestrainedWidth.new(
        original: 3840,
        implicit_limit: 1280
      )
      @restrained_height = RestrainedHeight.new(
        original: 2160,
        implicit_limit: 1080
      )

      @restrained_dimensions = RestrainedDimensions.new(
        width: @restrained_width,
        height: @restrained_height
      )
    end

    it "should be restrained" do
      assert @restrained_dimensions.restrained?
    end

    describe "restrained width" do
      it "should release restrain of height" do
        refute @restrained_dimensions.height.restrained?
      end

      it "should drop height#to_arg to its default" do
        _(@restrained_dimensions.height.to_arg).must_equal(-1)
      end

      it "but it should keep height#value restrained" do
        _(@restrained_dimensions.height.value).must_equal 1080
      end
    end
  end

  describe "implicitly restrained portrait video" do
    before do
      @restrained_width = RestrainedWidth.new(
        original: 2160,
        implicit_limit: 1280
      )
      @restrained_height = RestrainedHeight.new(
        original: 3840,
        implicit_limit: 1080
      )

      @restrained_dimensions = RestrainedDimensions.new(
        width: @restrained_width,
        height: @restrained_height
      )
    end

    it "should be restrained" do
      assert @restrained_dimensions.restrained?
    end

    describe "restrained height" do
      it "should restrain height" do
        assert @restrained_dimensions.height.restrained?
      end

      it "should drop width#to_arg to its default" do
        _(@restrained_dimensions.width.to_arg).must_equal(-1)
      end

      it "but it should keep width#value restrained" do
        _(@restrained_dimensions.width.value).must_equal 1280
      end
    end
  end
end
