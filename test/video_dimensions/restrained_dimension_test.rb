# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/video_dimensions/restrained_dimension"

describe RestrainedDimension do

  describe :value do
    describe "when explicit value provided" do
      it "should pick meaningful explicit value" do
        _(RestrainedDimension.new(
          original: 600,
          explicit_limit: 500,
          implicit_limit: 700
        ).value).must_equal 500
      end
    end

    describe "when explicit_limit is absent" do
      describe "when original is less then implicit_limit and crop_limit" do
        it "should be picked" do
          _(RestrainedDimension.new(
            original: 600,
            implicit_limit: 700,
            crop_limit: 800
          ).value).must_equal 600
        end
      end

      describe "when crop_limit is less then original and implicit_limit" do
        it "should be picked" do
          _(RestrainedDimension.new(
            original: 600,
            implicit_limit: 700,
            crop_limit: 500
          ).value).must_equal 500
        end
      end

      describe "when implicit_limit is less then original and crop_limit" do
        it "should be picked" do
          _(RestrainedDimension.new(
            original: 600,
            implicit_limit: 400,
            crop_limit: 500
          ).value).must_equal 400
        end
      end
    end

    describe "when explicit_limit is nil" do
      it "should pick minimum of original and implicit_limit" do
        _(RestrainedDimension.new(
          original: 600,
          implicit_limit: 700
        ).value).must_equal 600
      end
    end
  end

  describe :restrained? do
    it "should be true" do
      assert RestrainedDimension.new(
        original: 600,
        explicit_limit: 500,
        implicit_limit: 700
      ).restrained?
    end

    it "should be false when explicetly released" do
      rd = RestrainedDimension.new(
        original: 600,
        explicit_limit: 500,
        implicit_limit: 700
      )
      rd.release
      refute rd.restrained?
    end

    it "should be true" do
      assert RestrainedDimension.new(
        original: 600,
        implicit_limit: 500
      ).restrained?
    end

    it "should be false unless explicit_limit is set" do
      refute RestrainedDimension.new(
        original: 600,
        implicit_limit: 700
      ).restrained?
    end
  end

  describe :to_arg do
    it "should return value if it was restrained'" do
      d = RestrainedDimension.new(
        original: 600,
        implicit_limit: 500
      )
      _(d.to_arg).must_equal d.value
    end

    it "should return -1 if it wasn`t restrained'" do
      _(RestrainedDimension.new(
        original: 600,
        implicit_limit: 600
      ).to_arg).must_equal(-1)
    end
  end

end
