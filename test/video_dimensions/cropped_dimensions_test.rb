# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../lib/video_dimensions/cropped_dimensions"

def helper(ff_width:, ff_height:, width: nil, height: nil,
           crop_width: nil, crop_height: nil)

  original_dimensions = Struct.new(:width, :height).new(ff_width, ff_height)
  crop = Struct.new(:width, :height).new(crop_width, crop_height)
  restrained_dimensions = Struct.new(
    :restrained_width, :restrained_height
  ).new(width, height)

  CroppedDimensions.new(
    original_dimensions: original_dimensions,
    restrained_dimensions: restrained_dimensions,
    crop: crop,
  ).to_s
end

describe "CroppedDimensions#to_s" do
  describe "without cropped and limited sides" do
    before { @vd = helper(ff_width: 1280, ff_height: 720) }

    it "should return original dimensions" do
      _(@vd).must_equal "1280×720"
    end
  end

  describe "with limited width" do
    before { @vd = helper(ff_width: 1280, ff_height: 720, width: 960) }

    it "should recalculate sides" do
      _(@vd).must_equal "960×540"
    end
  end

  describe "with limited height" do
    before { @vd = helper(ff_width: 1280, ff_height: 720, height: 540) }

    it "should recalculate sides" do
      _(@vd).must_equal "960×540"
    end
  end

  describe "with limited width longer than the original one and non-limited height" do
    before { @vd = helper(ff_width: 1280, ff_height: 720, width: 1500) }

    it "limited width should take advantage and recalculate the height" do
      _(@vd).must_equal "1500×844"
    end
  end

  describe "with cropped sides" do
    before do
      @vd = helper(ff_width: 1280, ff_height: 720,
                   crop_width: 1000, crop_height: 800)
    end

    it "should consider if a side is already shorter" do
      _(@vd).must_equal "1000×720"
    end
  end

  describe "with cropped sides" do
    before do
      @vd = helper(ff_width: 1920, ff_height: 1080,
                   crop_width: 1079, crop_height: 1079)
    end

    it "should crop to previous even side" do
      _(@vd).must_equal "1078×1078"
    end
  end

  describe "with upscaled cropped sides" do
    before do
      @vd = helper(ff_width: 1280, ff_height: 720,
                   crop_width: 504, crop_height: 479, width: 600)
    end

    it "should crop to previous even side" do
      _(@vd).must_equal "600×569"
    end
  end

  describe "with both limited and cropped sides" do
    before do
      @vd = helper(ff_width: 1920, ff_height: 1080,
                   crop_width: 1280, crop_height: 720,
                   width: 960)
    end

    it "should apply the crop and then the side limit" do
      _(@vd).must_equal "960×540"
    end
  end
end
