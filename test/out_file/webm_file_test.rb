# frozen_string_literal: true

require "minitest/autorun"
require_relative "../../init"

describe WEBMFile do
  before { @webm_file = WEBMFile.new("foo", "bar.webm") }

  describe :to_s do
    it "should return filename as a string" do
      _(@webm_file.to_s).must_equal "foo-bar.webm"
    end
  end

  describe :size do
    it "should return the stubbed filesize" do
      @webm_file.stub :size, 250 do
        _(@webm_file.size).must_equal 250
      end
    end
  end

  describe :filesize do
    it "should count bytes correctly" do
      @webm_file.stub :size, 250 do
        _(@webm_file.filesize.call).must_equal "250 B"
      end
    end

    it "should count kilobytes correctly" do
      @webm_file.stub :size, 250_000 do
        _(@webm_file.filesize.call).must_equal "244.14 KiB"
      end
    end

    it "should count megabytes correctly" do
      @webm_file.stub :size, 3_000_000 do
        _(@webm_file.filesize.call).must_equal "2.86 MiB"
      end
    end
  end
end
