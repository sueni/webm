# frozen_string_literal: true

module NumericExtensions
  refine Numeric do
    def percent
      "#{(self * 100).round(1)}%"
    end

    def to_span
      Time.at(self).utc.strftime("%M:%S")
    end
  end
end
