#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative "init"

module Webm
  autoload :Amix, "cmd/amix/amix"
  autoload :Audio, "cmd/audio/audio"
  autoload :Convert, "cmd/convert/convert"
  autoload :Frame, "cmd/frame/frame"
  autoload :Info, "cmd/info/info"
  autoload :Join, "cmd/join/join"
  autoload :Mute, "cmd/mute/mute"
  autoload :Part, "cmd/part/part"
  autoload :Rename, "cmd/rename/rename"
  autoload :Silent, "cmd/silent/silent"
  autoload :Title, "cmd/title/title"
  autoload :Voice, "cmd/voice/voice"

  catch :halt do
    $command = CLI.parse

    use_stored_positions = %i[
      audio convert part silent voice
    ].include? $command

    input_files = InputFiles::Composer.new do |comp|
      comp.argv = ARGV
      comp.video_file = $opt.video_file
      comp.audio_file = $opt.audio_file
      comp.stored_file = PositionsStore.file if use_stored_positions
    end

    self.const_get($command.capitalize)::Cmd.new(input_files).call
  end&.then { |value| abort(value) if value.is_a? String }
end
