# frozen_string_literal: true

require_relative "area_overlay"
require_relative "overlay_position"

class OverlayManager
  attr_reader :bitrate_factor

  def initialize(input_file:, position:, crop:)
    @input_file = input_file
    @position = position
    @crop = crop
  end

  def area_overlay
    AreaOverlay.new(
      fg: fg_area,
      bg: bg_area,
      position: OverlayPosition.new(@position)
    )
  end

  def fg_area
    Area.from_xywh @crop
  end

  def bg_area
    Area.new w: @input_file.width, h: @input_file.height
  end

  def configure_bitrate_factor(pixelfactor:, blur:)
    pixelfactor ||= 1
    @bitrate_factor = overlay_mult * pixelfactor * blurfactor(blur)
  end

  def overlay_mult
    return 1 unless @crop
    1 - square(fg_area).fdiv(square bg_area)
  end

  def blurfactor(value)
    return 1 unless value
    value.fdiv 14
  end

  def square(area)
    area.width * area.height
  end
end
