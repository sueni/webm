# frozen_string_literal: true

class AreaOverlay
  attr_reader :bitrate_factor

  def initialize(fg:, bg:, position:)
    @fg_area = fg
    @bg_area = bg
    @position = position
    @x, @y = calculate
  end

  def calculate
    case @position.point
    when 1 then [shifted_x(x1), shifted_y(y1)]
    when 2 then [shifted_x(x2), shifted_y(y1)]
    when 3 then [shifted_x(x3), shifted_y(y1)]
    when 4 then [shifted_x(x1), shifted_y(y2)]
    when 5 then [shifted_x(x2), shifted_y(y2)]
    when 6 then [shifted_x(x3), shifted_y(y2)]
    when 7 then [shifted_x(x1), shifted_y(y3)]
    when 8 then [shifted_x(x2), shifted_y(y3)]
    when 9 then [shifted_x(x3), shifted_y(y3)]
    else [@fg_area.x_offset, @fg_area.y_offset]
    end
  end

  def to_s
    "#{@x}:#{@y}"
  end

private

  def shifted_x(position)
    position + @position.x_shift
  end

  def shifted_y(position)
    position + @position.y_shift
  end

  def x1; 0; end
  def x2; (@bg_area.width - @fg_area.width) / 2; end
  def x3; @bg_area.width - @fg_area.width; end

  def y1; @bg_area.height - @fg_area.height; end
  def y2; (@bg_area.height - @fg_area.height) / 2; end
  def y3; 0; end
end
