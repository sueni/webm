# frozen_string_literal: true

class OverlayPosition
  attr_reader :point, :x_shift, :y_shift

  def initialize(position_spec)
    parse position_spec
    @point   ||= 0
    @x_shift ||= 0
    @y_shift ||= 0
  end

  def parse(position_spec)
    @point, @x_shift, @y_shift =
      position_spec.split(/[,:;]/).map(&:to_i)
  end
end
