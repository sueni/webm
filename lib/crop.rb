# frozen_string_literal: true

require "forwardable"
require_relative "ui/positions_store"

class Crop
  include Comparable

  extend Forwardable
  def_delegators :@area, :width, :height, :x, :y

  def initialize(area)
    @area = area
  end

  def to_arg
    "crop=#{@area}"
  end

  def <=>(other)
    width * height <=> other.width * other.height
  end
end
