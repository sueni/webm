# frozen_string_literal: true

module FFprobe
  class Sorter
    def initialize(input_files)
      @input_files = input_files
    end

    def visuals
      explicit_visual&.then { |v| [v] } or
        @input_files.select { |f| f.visual? }
          .reject { |f| f.path == explicit_audible&.path }
    end

    def audibles
      explicit_audible&.then { |a| [a] } or
        @input_files.select { |f| f.audible? }
          .reject { |f| f.path == explicit_visual&.path }
    end

    def visual_and_audible
      v = visuals
      a = audibles

      case [v.size, a.size]
      when [1, 1] then [*v, *a]
      when [2, 1] then [*(v - a), *a]
      when [1, 2] then [*v, *(a - v)]
      else
        throw :halt, format(
          "%s: v:%s a:%s",
          Color.err["Can't pick video and audio automatically"],
          v.size,
          a.size,
        )
      end
    end

  private

    def explicit_visual
      @explicit_visual ||=
        $opt.video_file&.then { |vf| InputFile.new(vf) }
    end

    def explicit_audible
      @explicit_audible ||=
        $opt.audio_file&.then { |af| InputFile.new(af) }
    end
  end
end
