# frozen_string_literal: true

module FFprobe
  class Format
    attr_reader :format

    def initialize(format)
      @format = format
    end

    def duration
      format[:duration].to_f.round(3)
    end

    def title
      format[:tags]&.dig :title
    end

    def tags
      format[:tags]&.transform_keys(&:downcase)
    end
  end
end
