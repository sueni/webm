# frozen_string_literal: true

require_relative "compatible"

module Containable
  def container_fit?(codec)
    Compatible.codecs_for(container)&.include? codec
  end

  def webm?
    container == :webm
  end

  def container
    path.extname.to_s.delete_prefix(".").to_sym
  end
end
