# frozen_string_literal: true

require_relative "stream"

module FFprobe
  class VideoStream < Stream
    def framerate
      Rational(stream.fetch(:avg_frame_rate)).to_f
    rescue ZeroDivisionError
      24.0
    end

    def height
      stream[:height].to_i
    end

    def width
      stream[:width].to_i
    end

    def vp9?
      codec == "vp9"
    end

    def crop
      "#{width}:#{height}:0:0"
    end

    def is_picture?
      %w[mjpeg png webp].include? codec
    end
  end
end
