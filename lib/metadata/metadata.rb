# frozen_string_literal: true

require "tempfile"
require_relative "editable_metadata"
require_relative "video"
require_relative "audio"
require_relative "metadata_parser"
require_relative "../role/cleanable"
require_relative "../role/sanitizeable"

module Metadata
  extend self
  extend EditableMetadata

  using Cleanable
  using Sanitizeable

  attr_reader :video, :audio

  @force_filename_usage = $opt.meta.from_filename

  def set_from(string)
    return unless string
    Parser.parse string
    self.video = Parser.unique_video
    self.audio = Parser.audio
  end

  def reset_edited_title
    @edited_title = nil
  end

  def audio=(string)
    @audio = Audio.new(string) if string
  end

  def video=(string)
    @video = Video.new(string.clean) if string
  end

  def filepath=(path)
    @filepath = path
  end

  def display(indent = 4)
    Color.dark.call(to_s.empty? ? "<empty>" : wrap(indent))
  end

  def to_s
    title.to_s
  end

  def to_arg
    return if title.nil?
    arg
  end

  def to_arg!
    throw :halt, Color.err["Provide metadata title."] if title.nil?
    arg
  end

private

  def wrap(indent)
    to_s.gsub /\A|\n/, "\n#{' ' * indent}"
  end

  def arg
    %W[-map_chapters -1
       -map_metadata -1
       -metadata title=#{title}]
  end

  def title
    explicit_title or implicit_title
  end

  def explicit_title
    $opt.meta.title or
      edited_title or
      (filename if $opt.meta.from_filename)
  end

  def implicit_title
    Hash.new.tap do |s|
      s[:video] = configured_implicit_video if implicit_video
      s[:audio] = audio.to_meta if audio
    end.values.join("\n")
  end

  def configured_implicit_video
    implicit_video.use_mark if audio
    implicit_video.to_meta
  end

  def implicit_video
    video or video_from_filename
  end

  def video_from_filename
    Video.new filename if @filepath
  end

  def filename
    File.basename(@filepath, ".*").clean.desanitize
  end
end
