# frozen_string_literal: true

module Metadata
  module Parser
    extend self

    def parse(title)
      @lines = title.split "\n"
    end

    def audio
      @audio ||= find(Audio.mark)
    end

    def unique_video
      video unless video == audio
    end

  private

    def video
      @video ||= find(Video.mark) or
        strip_prefixes(@lines.first)
    end

    def find(type)
      @lines.find { |l| l.start_with? type }
        &.then(&method(:strip_prefixes))
    end

    def strip_prefixes(string)
      string&.delete_prefix(Video.mark)
        .delete_prefix(Audio.mark)
        .strip
    end

    refine String do
      def to_filename
        lines = self.split("\n")
        (lines.find { |c| c.start_with? Video.mark } or lines.first)
          .delete_prefix(Video.mark)
          .delete_prefix(Audio.mark)
      end
    end
  end
end
