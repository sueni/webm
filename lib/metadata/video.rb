# frozen_string_literal: true

module Metadata
  class Video
    def self.mark
      "☉: "
    end

    attr_reader :title

    def initialize(title)
      @title = title
    end

    def to_meta
      title
    end

    def use_mark
      mark = self.class.mark
      @title = mark + title.delete_prefix(mark)
    end
  end
end
