# frozen_string_literal: true

module Metadata
  class Audio
    def self.mark
      "♫: "
    end

    attr_reader :title

    def initialize(title)
      @title = title
    end

    def to_meta
      title&.then { |t| "#{self.class.mark}#{t}" }
    end
  end
end
