# frozen_string_literal: true

class FadeAudio
  def initialize(filtergraph)
    choose_method_of_fading filtergraph || 0.5
  end

  def duration(slice_duration)
    @slice_duration = slice_duration
    self
  end

  def to_arg
    raise "FadeAudio: need to set duration first" unless @slice_duration
    send @method
  end

  private

  def choose_method_of_fading(filtergraph)
    @method, @filtergraph =
      case filtergraph.to_s
      when /^\d+(?:\.\d+)?$/
        [:symmetric_fade, filtergraph.to_f]
      when /(?:\bin=\d+(?:\.\d+)?)|(?:\bout=\d+(?:\.\d+)?)/
        [:asymmetric_fade, filtergraph]
      else
        throw :halt, Color.err["Wrong fade audio argument format."]
      end
  end

  def symmetric_fade
    format(
      "%s,%s",
      fade_in(@filtergraph),
      fade_out(@slice_duration - @filtergraph, @filtergraph)
    )
  end

  def asymmetric_fade
    pieces = []

    if /\bin=(?<to>\d+(?:\.\d+)?)/ =~ @filtergraph
      pieces << fade_in(to) if to.to_f > 0
    end

    if /\bout=(?<to>\d+(?:\.\d+)?)/ =~ @filtergraph
      to = to.to_f
      pieces << fade_out(@slice_duration - to, to) if to > 0
    end

    pieces.join(",")
  end

  def fade_in(to)
    format("afade=t=in:ss=0:d=%g", to)
  end

  def fade_out(from, to)
    format("afade=t=out:st=%g:d=%g", from, to)
  end
end
