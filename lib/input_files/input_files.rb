# frozen_string_literal: true

require_relative "input_file"
require_relative "composer"
require_relative "checker"
require_relative "rule"
