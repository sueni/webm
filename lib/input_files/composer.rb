# frozen_string_literal: true

module InputFiles
  class Composer

    attr_writer :argv, :video_file, :audio_file, :stored_file

    def initialize
      @stored_file = nil
      yield self
    end

    def files
      [@video_file, @audio_file].concat(@argv)
        .uniq
        .tap { |files| files << @stored_file if files.none? }
        .compact
        .tap { |files| files.each(&method(:check_existence)) }
    end

    def apply(rule)
      Checker.new(rule).verify(files)
    end

    def stored_file_only?
      @argv.empty? and !!@stored_file
    end

  private

    def check_existence(file)
      throw :halt, format(
        "%s: %s",
        Color.err["File not exists"],
        file
      ) unless File.exist? file
    end
  end
end
