# frozen_string_literal: true

require_relative "../role/singular_range"

class Rule
  using SingularRange

  attr_reader :amount, :filter

  def initialize(filter = {})
    @filter = filter
    @amount = nil
    yield self if block_given?
  end

  def limit_amount(range)
    @amount = range
    self
  end

  def singular?
    singular_amount? or singular_filter?
  end

private

  def singular_amount?
    !!@amount&.singular?
  end

  def singular_filter?
    return false if @filter.empty?

    @filter.size == 1 and
      @filter.values.last.then(&:singular?)
  end
end
