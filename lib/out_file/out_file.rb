# frozen_string_literal: true

require "pathname"
require "fileutils"
require_relative "file_summary"
require_relative "../role/colorizeable"
require_relative "../role/human_readable"

class OutFile
  include Identifiable
  using HumanReadable
  using Colorizeable

  SPACER = '-'

  def initialize(*name_chunks, ext:, dir: ".")
    @name_chunks = name_chunks
    @ext = ext
    @dir = Pathname dir
    @passlog_name_chunks = name_chunks.clone

    if $opt.filename_suffix
      @name_chunks << $opt.filename_suffix
    end
  end

  def summary(message, **args)
    puts FileSummary.string(
      message: Color.succ[message],
      filename: name.ls_color,
      filesize: filesize.call,
      metadata: Metadata.display(message.size + 2),
      **args
    )
  end

  def method_missing(m, *args, &block)
    ffprobe.public_send m, *args, &block
  end

  def ==(other)
    self.path == other.path
  end

  def ffprobe
    @ffprobe ||= FFprobe::Build.new(path)
  end

  def to_s
    path.to_s
  end

  alias_method :to_str, :to_s

  def rename(to:)
    FileUtils.move path, to
  end

  def exists?
    path.exist?
  end

  def filesize
    proc { Color.fs[size.to_filesize] }
  end

  def size
    size? or 0
  end

  def size?
    path.size?
  end

  def unlink
    path.unlink if path.exist?
  end

  def path
    @path ||= (name.absolute? ? name : @dir.join(name))
  end

  def name
    @name ||= Pathname.new(
      @name_chunks.join(SPACER).chomp(@ext) + @ext
    )
  end

  def basename
    name.basename(".*")
  end

  def passlog_name
    Pathname.new(@passlog_name_chunks.join(SPACER)).basename()
  end

  def container
    @ext.delete_prefix(".").to_sym
  end
end
