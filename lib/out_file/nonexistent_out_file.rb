# frozen_string_literal: true

class NullOutFile
  def method_missing(*)
  end
end
