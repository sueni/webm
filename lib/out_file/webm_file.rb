# frozen_string_literal: true

require_relative "out_file"
require_relative "nonexistent_out_file"

class WEBMFile < OutFile
  EXT = ".webm"

  def initialize(*name_chunks)
    super(*name_chunks, ext: EXT)
  end
end
