# frozen_string_literal: true

require_relative "out_file"

class MP4File < OutFile
  def initialize(*name_chunks)
    super(*name_chunks, ext: ".mp4")
  end
end
