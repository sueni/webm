# frozen_string_literal: true

class FLACFile < OutFile
  def initialize(*name_chunks)
    super(*name_chunks, ext: ".flac")
  end
end
