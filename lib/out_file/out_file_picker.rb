# frozen_string_literal: true

require_relative "../../extensions/array_extensions"

module OutFilePicker
  using ArrayExtensions

  def self.call(extensions)
    case (ext = extensions.single)
    when ".avi" then MP4File
    when ".flac" then FLACFile
    when ".mkv" then MP4File
    when ".mp4" then MP4File
    when ".webm" then WEBMFile
    when ".wmv" then WMVFile
    else
      throw :halt, format(
        "%s: %s",
        Color.err["Unsupported extension"],
        ext
      )
    end
  end
end
