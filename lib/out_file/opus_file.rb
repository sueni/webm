# frozen_string_literal: true

class OPUSFile < OutFile
  def initialize(*name_chunks)
    super(*name_chunks, ext: ".opus")
  end
end
