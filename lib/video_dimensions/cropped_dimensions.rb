# frozen_string_literal: true

class CroppedDimensions
  def initialize(original_dimensions:, restrained_dimensions:, crop:)
    @original_width = original_dimensions.width
    @original_height = original_dimensions.height
    @restrained_width = restrained_dimensions.restrained_width
    @restrained_height = restrained_dimensions.restrained_height
    @crop = crop
  end

  def to_s
    "#{effective_width}×#{effective_height}"
  end

  def assumed_bitrate
    (effective_width + effective_height) / 2 / square_factor
  end

  def minimal_side_length
    [effective_width, effective_height].min
  end

private

  def square_factor
    min, max = [effective_width, effective_height].sort!
    Math.sqrt(max.fdiv min)
  end

  def effective_width
    @restrained_width or
      (cropped_width / height_scale_ratio).round(half: :down)
  end

  def effective_height
    @restrained_height or
      (cropped_height / width_scale_ratio).round(half: :down)
  end

  def width_scale_ratio
    cropped_width.fdiv(@restrained_width || cropped_width)
  end

  def height_scale_ratio
    cropped_height.fdiv(@restrained_height || cropped_height)
  end

  def cropped_width
    [adjusted_cropped_side(@crop.width), @original_width].compact.min
  end

  def cropped_height
    [adjusted_cropped_side(@crop.height), @original_height].compact.min
  end

  def adjusted_cropped_side(side)
    return nil unless side
    side / 2 * 2
  end
end
