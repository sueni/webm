# frozen_string_literal: true

require_relative "restrained_width"
require_relative "restrained_height"

class RestrainedDimensions
  attr_reader :width, :height

  def initialize(width:, height:)
    @short_side, @long_side = [
      @width = width,
      @height = height,
    ].sort_by(&:original_value)

    @short_side.release if @long_side.restrained?
  end

  def restrained?
    [@width, @height].any?(&:restrained?)
  end

  def restrained_width
    @width.value if @width.restrained?
  end

  def restrained_height
    @height.value if @height.restrained?
  end

  def swap
    @width, @height = @height, @width
  end
end
