# frozen_string_literal: true

class RestrainedDimension
  attr_reader :original_value

  def initialize(**args)
    @original_value = args[:original]
    @explicit_limit = args[:explicit_limit]
    @implicit_limit = args[:implicit_limit]
    @crop_limit = args[:crop_limit]
    @released = false
  end

  def value
    @explicit_limit or limit(
      implicitly_limited(@original_value),
      crop_limited(@original_value)
    )
  end

  def to_arg
    restrained? ? value : -1
  end

  def restrained?
    return false if @released

    explicitly_limited? or
      implicitly_limited(@original_value).<(
        crop_limited(@original_value)
      )
  end

  def release
    @released = true
  end

private

  def explicitly_limited?
    @explicit_limit and
      @explicit_limit != @original_value
  end

  def implicitly_limited(value)
    limit @implicit_limit, value
  end

  def crop_limited(value)
    limit @crop_limit, value
  end

  def limit(value, limit)
    [limit, value].compact.min
  end
end
