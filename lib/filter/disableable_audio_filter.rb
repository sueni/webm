# frozen_string_literal: true

require_relative "audio_filter"

class DisableableAudioFilter < AudioFilter
  def disable
    @disabled = true
  end

  def to_a
    return ["-an"] if @disabled
    super
  end
end
