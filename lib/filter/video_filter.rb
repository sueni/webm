# frozen_string_literal: true

require_relative "filter"

module SimpleVideoFilter
  def prefix
    "-vf"
  end
end

class VideoFilter < Filter
  prepend SimpleVideoFilter if $opt.preview

  def prefix
    ComplexFilter::Build::PREFIX
  end
end
