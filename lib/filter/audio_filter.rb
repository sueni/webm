# frozen_string_literal: true

require_relative "filter"

class AudioFilter < Filter
  def prefix
    "-af"
  end
end

