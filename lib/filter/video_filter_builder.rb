# frozen_string_literal: true

require_relative "../role/streamable"

class VideoFilterBuilder
  include Streamable

  def initialize(**args)
    @input_file = args[:input_file]
    @span = args[:span]
    @crop = args[:crop]
    @pixelize = args[:pixelize]
    @restrained_dimensions = args[:restrained_dimensions]
    @overlay_manager = args[:overlay_manager]
    @sharped = false
  end

  def call
    VideoFilter.new do |vf|
      vf << overlay if $opt.overlay
      vf.concat $opt.delogo.to_arg
      if @crop.is_a?(Crop)
        vf << @crop.to_arg
        vf << sharp_once
      end
      vf << fade_video if $opt.fade_video
      vf << rotate if $opt.rotate
      if @restrained_dimensions.restrained?
        vf << scale
        vf << sharp_once
      end
      vf << hardsub_internal if $opt.hardsub_internal
      vf << hardsub_external if $opt.hardsub_external
      vf << $opt.logo.to_arg if $opt.logo.content
      vf << $opt.speed.to_s if $opt.speed.active?
      vf << @pixelize.to_s if @pixelize.active? and not $opt.overlay
      vf << blur if $opt.blur.active? and not $opt.overlay
      vf.concat $opt.custom_vf
      vf << minterpolate if $opt.speed.slow?
    end
  end

private

  def minterpolate
    "minterpolate=fps=25:scd=none:me_mode=bidir:vsbmc=1:search_param=400"
  end

  def blur
    "boxblur=#{$opt.blur}"
  end

  def overlay
    mode = case
           when @pixelize.active? then @pixelize
           when $opt.blur then blur
           else blur
           end
    crop = Crop.new @overlay_manager.fg_area
    format(
      "[0:v]#{mode}[bg];[0:v]%s[fg];[bg][fg]overlay=%s",
      crop.to_arg,
      @overlay_manager.area_overlay.to_s
    )
  end

  def rotate
    $opt.rotate.map { "transpose=#{_1}" }.join(',')
  end

  def fade_video
    format(
      "fade=out:st=%s:d=%s",
      @span.seconds - $opt.fade_video,
      $opt.fade_video
    )
  end

  def scale
    format(
      "scale=%s:%s,setsar=1:1",
      @restrained_dimensions.width.to_arg,
      @restrained_dimensions.height.to_arg
    )
  end

  def sharp_once
    return if @sharped
    @sharped = true
    "unsharp=luma_msize_x=5:luma_msize_y=5:luma_amount=1.5"
  end

  def hardsub_internal
    return if $opt.hardsub_external

    extractor = Subtitles::Extract::Internal::Cmd.new(
      container: @input_file,
      stream_id: subtitle_stream($opt.hardsub_internal),
      start_position: @span.start_position,
      seconds: @span.seconds,
    )

    Subtitles::String.from(extractor.call)
  end

  def hardsub_external
    extractor = Subtitles::Extract::External::Cmd.new(
      subs_file: $opt.hardsub_external,
      start_position: @span.start_position,
      seconds: @span.seconds,
    )

    Subtitles::String.from(extractor.call)
  end
end
