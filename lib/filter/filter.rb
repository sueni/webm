# frozen_string_literal: true

require "forwardable"

class Filter
  extend Forwardable
  def_delegators :@arguments, :<<, :concat

  def initialize(arg = nil)
    @arguments = []
    @arguments << arg if arg
    yield self if block_given?
  end

  def prefix
    raise NotImplementedError,
      "#{self.class} should implement #{__callee__}"
  end

  def to_a
    return [] if @arguments.none?
    [prefix, *@arguments.compact.join(",")]
  end
end
