# frozen_string_literal: true

require "optparse"
require_relative "audio_bitrate"
require_relative "blur"
require_relative "speed"
require_relative "logo"
require_relative "delogo"
require_relative "sub"
require_relative "crop"
require_relative "keyframe"

module CLI
  @commands = Struct.new(*%i[amix audio convert frame info join mute part rename silent title voice]).new

  $opt = Struct.new(*%i[
    audio_bitrate
    audio_file
    back_volume
    blink_frame
    blur
    crop
    custom_vf
    cycle
    delogo
    detect_crop
    dry_run
    fade_audio
    fade_video
    filename_suffix
    frame_rate
    hardsub_external
    hardsub_internal
    height
    info_only
    keyframe
    logo
    long
    map_streams
    meta
    mute
    overlay
    pixels_per_side
    preview
    real_run
    report_meta
    rotate
    span
    speed
    sub
    target_file_size
    video_bitrate
    video_file
    volume
    width
    zoom
  ], keyword_init: true).new(
    audio_bitrate: AudioBitrate.new(64),
    blur: Blur.new(20),
    crop: Crop.new,
    custom_vf: [],
    cycle: 1,
    delogo: Delogo.new,
    keyframe: Keyframe.new,
    logo: Logo.new,
    meta: Struct.new(*%i[edit from_filename set_audio title], keyword_init: true).new(set_audio: true),
    span: Struct.new(*%i[duration start_at_beginning start_position stop_at_end stop_position]).new,
    speed: Speed.new(1.0),
    sub: Sub.new
  )

  @commands.amix = OptionParser.new do |opts|
    opts.banner = "Usage: amix [options] [-a] audio-file [-v] video-file (any order)"
    opts.on("-B BITRATE", Integer, "Audio bitrate") { |o| $opt.audio_bitrate.set o }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-V VOL", Float, "Foreground volume (factor, 1.0)") { |o| $opt.volume = o }
    opts.on("--bv VOL", Float, "Background volume (factor, 0.1)") { |o| $opt.back_volume = o }
    opts.separator "Positions"
    opts.on("-0", "Mix audio file from the start") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Mix audio file till the end") { $opt.span.stop_at_end = true }
    opts.on("-e", "Mix entire audio file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("--start SEC", Float, "Start position in seconds") { |o| $opt.span.start_position = o }
    opts.on("--stop SEC", Float, "End position in seconds") { |o| $opt.span.stop_position = o }
    opts.on("--duration SEC", Float, "Result duration") { |o| $opt.span.duration = o }
    opts.separator "TITLE"
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
    opts.separator "INPUT"
    opts.on("-a FILE", "Background audio file") { |o| $opt.audio_file = o }
    opts.on("-v FILE", "Foreground file") { |o| $opt.video_file = o }
  end

  @commands.audio = OptionParser.new do |opts|
    opts.on("-B BITRATE", Integer, "Audio bitrate") { |o| $opt.audio_bitrate.set o }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-e", "Convert entire file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("-f [SEC|in=SEC:out=SEC]", "Fade audio in and out (def.: 0.5)") { |o| $opt.fade_audio = FadeAudio.new(o) }
    opts.on("-m STREAMS", "Map streams to output") { |o| $opt.map_streams = o }
    opts.on("-V VOL", "Change volume (factor, 1.0)") { |o| $opt.volume = o }
    opts.separator "Positions"
    opts.on("--start SEC", Float, "Start position in seconds") { |o| $opt.span.start_position = o }
    opts.on("--stop SEC", Float, "End position in seconds") { |o| $opt.span.stop_position = o }
    opts.on("--duration SEC", Float, "Result duration") { |o| $opt.span.duration = o }
  end

  @commands.convert = OptionParser.new do |opts|
    opts.banner = "Usage: convert [options] file"
    opts.on("-S SPEED", Numeric, "Speed multiplier") { |o| $opt.speed.set 1.fdiv(o) }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-e", "Convert entire file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("-s SIZE", "Limit file's size (suffixes: k, m)") { |o| $opt.target_file_size = o }
    opts.on("-V VOL", "Change volume (factor, 1.0)") { |o| $opt.volume = o }
    opts.separator "Video mutation"
    opts.on("--vf GRAPH", "Append custom video filter") { |o| $opt.custom_vf << o }
    opts.on("-r RATE", Integer, "Frame rate") { |o| $opt.frame_rate = o }
    opts.on("-p [NUMBER]", Integer, "Pixelize video (def.: 42)") { $opt.pixels_per_side = _1 || 42; $opt.blur.drop }
    opts.on("--blur [STRENGTH]", Integer, "Blur video (def.: 20)") { $opt.blur.set _1; $opt.pixels_per_side = nil}
    opts.on("--overlay [POINT, X_SHIFT, Y_SHIFT]", "Overlay blured|pixelized video with intact area (crop is used)") { $opt.overlay = _1 || "0" }
    opts.on("--deinterlace", "Apply deinterlace filter") { |o| $opt.custom_vf << "yadif" }
    opts.on("--rotate-cw", "Rotate video clockwise") { ($opt.rotate ||= []) << 1 }
    opts.on("--rotate-ccw ", "Rotate video counterclockwise") { ($opt.rotate ||= []) << 2 }
    opts.separator "Bitrate"
    opts.on("-B BITRATE", Integer, "Audio bitrate") { |o| $opt.audio_bitrate.set o }
    opts.on("-b BITRATE", Integer, "Video bitrate") { |o| $opt.video_bitrate = o }
    opts.separator "Crop"
    opts.on("-c [x,y,w,h]", "Crop the frame") { |o| $opt.crop.set o }
    opts.on("-C [low|med|hi]", "Auto detect crop", %w[low med hi]) { |o| $opt.detect_crop = o&.to_sym || :med }
    opts.separator "Fade"
    opts.on("-f [SEC|in=SEC:out=SEC]", "Fade audio in and out (def.: 0.5)") { |o| $opt.fade_audio = FadeAudio.new(o) }
    opts.on("-F [SEC]", Float, "Fade video out into black (def.: 0.5)") { |o| $opt.fade_video = o || 0.5 }
    opts.separator "Info"
    opts.on("-i", "Display summary and exit") { $opt.info_only = true }
    opts.on("-I", "Preview the result") { $opt.preview = true }
    opts.separator "Logo"
    opts.on("-l [x,y,w,h]", "Remove logo") { |o| $opt.delogo << o }
    opts.on("--logo TEXT|IMAGE", "Draw logo") { |o| $opt.logo.content = o }
    opts.on("--logo-alpha LVL", "Alpha-level of logo (0.0-1.0, def.: #{$opt.logo.alpha})") { |o| $opt.logo.alpha = o }
    opts.on("--logo-color COLOR", "Logo font color (def.: #{$opt.logo.color})") { |o| $opt.logo.color = o }
    opts.on("--logo-font NAME", "Logo font (def.: #{$opt.logo.font})") { |o| $opt.logo.font = o }
    opts.on("--logo-padding PIX", "Logo padding (def.: #{$opt.logo.padding})") { |o| $opt.logo.padding = o }
    opts.on("--logo-place POS", Integer, "Logo place (def.: #{$opt.logo.place})") { |o| $opt.logo.place = o }
    opts.on("--logo-size NUM", "Logo font size (def.: #{$opt.logo.size})") { |o| $opt.logo.size = o }
    opts.separator "Positions"
    opts.on("--start SEC", Float, "Start position in seconds") { |o| $opt.span.start_position = o }
    opts.on("--stop SEC", Float, "End position in seconds") { |o| $opt.span.stop_position = o }
    opts.on("--duration SEC", Float, "Result duration") { |o| $opt.span.duration = o }
    opts.separator "Resize"
    opts.on("-w PIXELS", Integer, "Width") { |o| $opt.width = o }
    opts.on("-H PIXELS", Integer, "Height") { |o| $opt.height = o }
    opts.separator "Streams"
    opts.on("-m STREAMS", "Map streams to output") { |o| $opt.map_streams = o }
    opts.on("-M", "Mute") { $opt.mute = true; $opt.audio_bitrate.set 0 }
    opts.separator "Subtitles"
    opts.on("-o STREAM", "Overlay with internal subtutles") { |o| $opt.hardsub_internal = o }
    opts.on("-O SUBS", "Overlay with external subtutles") { |o| $opt.hardsub_external = o }
    opts.on("--sub PRESET", %i[arial cambria cronos sans], "Subtitles preset (arial, cambria, cronos, sans)") { $opt.sub.preset_id = _1.to_sym }
    opts.on("--sub-alpha LVL", %w[1 2 3 4], "Alpha-level of subtitles (1-4)") { |o| $opt.sub.alpha = o.to_i }
    opts.on("--sub-bg RRGGBB", "Background color of subtitles") { |o| $opt.sub.bg = o }
    opts.on("--sub-edit", "Edit subtitles") { |o| $opt.sub.edit = o }
    opts.on("--sub-fg RRGGBB", "Foreground color of subtitles") { |o| $opt.sub.fg = o }
    opts.on("--sub-outline NUM", Integer, %w[0 1 2 3 4], "Outline width in px (0-4)") { |o| $opt.sub.outline = o }
    opts.on("--sub-shadow NUM", Integer, %w[0 1 2 3 4], "Depth of the shadow (0-4)") { |o| $opt.sub.shadow = o }
    opts.on("--sub-size NUM", "Font size for subtitles (def.: 20)") { |o| $opt.sub.size = o }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title") { |o| $opt.meta.title = o }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
    opts.on("-N", "Use filename as the title") { $opt.meta.from_filename = true }
  end

  @commands.frame = OptionParser.new do |opts|
    opts.banner = "Usage: frame [options] image"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-M", "Mute") { $opt.mute = true }
    opts.on("-b", "Make blink frame") { $opt.blink_frame = true }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
    opts.separator "RESIZE"
    opts.on("-w PIXELS", Integer, "Width") { |o| $opt.width = o }
    opts.on("-H PIXELS", Integer, "Height") { |o| $opt.height = o }
  end

  @commands.info = OptionParser.new

  @commands.join = OptionParser.new do |opts|
    opts.banner = "Usage: join [options] file1, file2..."
    opts.on("-c COUNT", Integer, "Cycle joined webm") { |o| $opt.cycle = o }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
  end

  @commands.mute = OptionParser.new do |opts|
    opts.banner = "Usage: mute [options] file"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
  end

  @commands.part = OptionParser.new do |opts|
    opts.banner = "Usage: part [options] file"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-M", "Mute") { $opt.mute = true }
    opts.separator "Positions"
    opts.on("-0", "Start at file's beginning") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Stop at file's ending") { $opt.span.stop_at_end = true }
    opts.on("--pk", "Start parting at previous keyframe") { $opt.keyframe.mode = :preceeding }
    opts.on("--nk", "Start parting at next keyframe") { $opt.keyframe.mode = :succeeding }
    opts.on("--start SEC", Float, "Start position in seconds") { |o| $opt.span.start_position = o }
    opts.on("--stop SEC", Float, "End position in seconds") { |o| $opt.span.stop_position = o }
    opts.on("--duration SEC", Float, "Result duration") { |o| $opt.span.duration = o }
    opts.separator "Title"
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
    opts.on("-N", "Use filename as the title") { $opt.meta.from_filename = true }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
  end

  @commands.rename = OptionParser.new do |opts|
    opts.banner = "Usage: rename [options] file"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-R", "Real run") { $opt.real_run = true }
  end

  @commands.silent = OptionParser.new do |opts|
    opts.banner = "Usage: silent [options] webm"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
  end

  @commands.title = OptionParser.new do |opts|
    opts.banner = "Usage: title [options] file(s)"
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-N", "Use filename as the title") { $opt.meta.from_filename = true }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
    opts.on("-e", "Edit tiles with editor one by one") { $opt.meta.edit = :single }
    opts.on("-E", "Edit tile with editor in bunch") { $opt.meta.edit = :bunch }
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
  end

  @commands.voice = OptionParser.new do |opts|
    opts.banner = "Usage: voice [options] [-a] audio-file [-v] video-file (any order)"
    opts.on("-B BITRATE", Integer, "Audio bitrate") { |o| $opt.audio_bitrate.set o }
    opts.on("-d", "Dry run") { $opt.dry_run = $opt.dry_run.to_i + 1 }
    opts.on("-f [SEC|[in=SEC:][out=SEC]]", "Fade audio in and out (def.: 0.5)") { |o| $opt.fade_audio = FadeAudio.new(o) }
    opts.on("-l", "Last untill the longest stream ends") { $opt.long = true }
    opts.on("-V VOL", "Change volume (factor, 1.0)") { |o| $opt.volume = o }
    opts.on("-z POINT", Integer, "Zoom to one of points") { |o| $opt.zoom = o }
    opts.separator "Positions"
    opts.on("-0", "Mix audio file from the start") { $opt.span.start_at_beginning = true }
    opts.on("-9", "Mix audio file till the end") { $opt.span.stop_at_end = true }
    opts.on("-e", "Mix entire audio file") { $opt.span.start_at_beginning = $opt.span.stop_at_end = true }
    opts.on("--start SEC", Float, "Start position in seconds") { |o| $opt.span.start_position = o }
    opts.on("--stop SEC", Float, "End position in seconds") { |o| $opt.span.stop_position = o }
    opts.on("--duration SEC", Float, "Result duration") { |o| $opt.span.duration = o }
    opts.separator "TITLE"
    opts.on("-t TITLE", "Set metadata title tag") { |o| $opt.meta.title = o }
    opts.on("-n", "Don't include audio metadata") { $opt.meta.set_audio = false }
    opts.on("-T", "Strip metadata title") { $opt.meta.title = "" }
    opts.separator "INPUT"
    opts.on("-a FILE", "Audio file") { |o| $opt.audio_file = o }
    opts.on("-v FILE", "Video file") { |o| $opt.video_file = o }
    opts.separator "RESIZE"
    opts.on("-w PIXELS", Integer, "Width") { |o| $opt.width = o }
    opts.on("-H PIXELS", Integer, "Height") { |o| $opt.height = o }
  end

  def self.commands_description
    %{Commands are:
    amix    > Merge external audio into existing one
    audio   > Evoke [and replace] audiotrack
    convert > Convert to webm
    frame   > Make one-frame webm out of image
    info    > Display a brief info about the media
    join    > Concatenate several webms into one
    mute    > Remove the audio track
    part    > Extract part of file
    rename  > Rename the file according to its metadata
    silent  > Silence the whole media or a region
    title   > Set metadata title tag to the webm
    voice   > Make a webm, merging a video file and an audio

    See "webm COMMAND -h" for more information on a specific command}
  end

  def self.parse_global_options
    parser = OptionParser.new do |opts|
      opts.banner = "Usage: webm COMMAND [options]"
      opts.separator ""
      opts.separator "Global options:"
      opts.on("--suf SUFFIX", "Append suffix to filename") { |o| $opt.filename_suffix = o }
      opts.separator commands_description
    end
    parser.order!
  rescue OptionParser::ParseError => e
    puts Color.err[e.message]
    throw :halt, parser.help
  end

  def self.parse_command
    @command = guess_command
    parser = @commands[@command]
    parser.parse!
  rescue OptionParser::ParseError => e
    puts Color.err[e.message]
    puts parser.help if parser
    exit 1
  end

  def self.guess_command
    command = ARGV.shift or
      fail(OptionParser::MissingArgument,
           Color.err["Provide a command:\n\n"] + commands_description)

    guesses = @commands.members.select { |c| c.match?(/^#{command}/) }

    case guesses.size
    when 0
      fail OptionParser::InvalidArgument, Color.err["Unknown command -> #{command}"]
    when 1
      guesses.first
    else
      fail OptionParser::AmbiguousArgument,
        Color.err["Umbiguous command -> "] +
        "#{command}, matched: [#{guesses.join(", ")}]"
    end
  end

  def self.parse
    parse_global_options
    parse_command
    @command
  end
end
