# frozen_string_literal: true

module CLI
  class Option
    def initialize(initial_value = nil)
      @initial_value = initial_value
      @active = initial_value
      @was_changed = false
    end

    def set(value)
      @active = value
      @was_changed = true
    end

    def value
      @active
    end

    def active?
      @was_changed
    end

    def passive?
      !@was_changed
    end

    def drop
      @active = @initial_value
      @was_changed = false
    end

    def to_s
      value.to_s
    end
  end
end
