# frozen_string_literal: true

module CLI
  Sub = Struct.new(*%i[alpha bg edit fg outline preset_id shadow size])
end
