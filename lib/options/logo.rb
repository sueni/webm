# frozen_string_literal: true

require "tempfile"

module CLI
  Logo = Struct.new(
    :alpha,
    :color,
    :place,
    :font,
    :content,
    :padding,
    :size,
    keyword_init: true
  ) do

    def initialize(**args)
      default_args = {
        alpha: 0.5,
        color: "white",
        place: 3,
        font: "cocogoose",
        padding: 2,
        size: 12,
      }.merge(args)

      super(**default_args)
    end

    def image?
      content and
        File.exist?(content) and
        File.extname(content) == ".png"
    end

    def to_arg
      image? ? image_logo : text_logo
    end

  private

    def image_logo
      # https://stackoverflow.com/questions/10918907/how-to-add-transparent-watermark-in-center-of-a-video-with-ffmpeg/10920872#10920872
      case place
      when 1 then "overlay=#{padding}:H-h-#{padding}"
      when 3 then "overlay=W-w-#{padding}:H-h-#{padding}"
      when 7 then "overlay=#{padding}:#{padding}"
      when 9 then "overlay=W-w-#{padding}:#{padding}"
      else throw :halt, Color.err["Unknown place for image logo."]
      end
    end

    def text_logo
      # https://superuser.com/a/939386

      place_spec = case place
                   when 1 then "x=#{padding}:y=h-th-#{padding}"
                   when 2 then "x=(w-text_w)/2:y=h-th-#{padding}"
                   when 3 then "x=w-tw-2-#{padding}:y=h-th-#{padding}"
                   when 5 then "x=(w-text_w)/2:y=(h-text_h)/2"
                   when 7 then "x=#{padding}:y=#{padding}"
                   when 8 then "x=(w-text_w)/2:y=#{padding}"
                   when 9 then "x=w-tw-#{padding}:y=#{padding}"
                   end

      String.new.tap do |s|
        s << "drawtext="
        s << ":#{place_spec}"
        s << ":font=#{font}"
        s << ":#{formatted_content}"
        s << ":fontsize=#{size}"
        s << ":fontcolor=#{color}@#{alpha}"
      end
    end

    def formatted_content
      return "text=#{content}" if image?

      "/tmp/webm-logo-text".then do |path|
        File.write path, content
        "textfile=#{path}"
      end
    end
  end
end
