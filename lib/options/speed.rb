# frozen_string_literal: true

require_relative "option"

module CLI
  class Speed < Option
    def to_s
      "setpts=#{value}*PTS"
    end

    def slow?
      self.value > 1
    end
  end
end
