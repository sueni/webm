# frozen_string_literal: true

require "forwardable"
require_relative "time_solver"
require_relative "visual_positions"
require_relative "duration"

class Span
  extend Forwardable

  attr_reader :start_position, :stop_position, :duration

  def_delegators :duration, :seconds, :result_seconds

  def initialize(edges)
    @edges = edges
    check_positions
    apply_positions
    check_stop_position_boundary
    VisualPositions.draw
  end

private

  def check_positions
    if [@edges.start, @edges.stop].none?
      throw :halt, Color.err["Neither start nor stop position provided."]
    end
  end

  def apply_positions
    time_solver = TimeSolver.new(
      start: @edges.start,
      stop: @edges.stop,
      duration: $opt.span.duration,
      implicit_start: method(:implicit_start_position),
      implicit_stop: method(:implicit_stop_position),
    )

    @start_position = Position.from_float time_solver.start
    @stop_position = Position.from_float time_solver.stop
    @duration = Duration.new time_solver.duration
  end

  def implicit_start_position
    VisualPositions.implicit_start = true
    0.0
  end

  def implicit_stop_position
    VisualPositions.implicit_stop = true
    @edges.entire_duration
  end

  def check_stop_position_boundary
    valid_range = 0..@edges.entire_duration

    unless valid_range.cover? stop_position.to_f
      throw :halt, Color.err["End position is out of file's length."]
    end
  end
end
