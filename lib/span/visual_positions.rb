# frozen_string_literal: true

require_relative "../role/once_runner"

module VisualPositions
  extend self
  extend OnceRunner

  attr_accessor :implicit_start
  attr_accessor :implicit_stop

  def draw
    return if $opt.dry_run

    run_once do
      print(String.new.tap do |s|
        s << (implicit_start ? Color.implicit : Color.explicit)["▬"]
        s << ($opt.span.duration ? Color.explicit : Color.inactive)["▬"]
        s << (implicit_stop ? Color.implicit : Color.explicit)["▬"]
        s << " "
      end)
    end
  end
end
