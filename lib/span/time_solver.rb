# frozen_string_literal: true

class TimeSolver
  attr_reader :start, :stop, :duration

  def initialize(**args)
    @start = args[:start]
    @stop = args[:stop]
    @duration = args[:duration]
    @implicit_start = args[:implicit_start] || proc {}
    @implicit_stop = args[:implicit_stop] || proc {}
    calculate
  end

  def calculate
    case
    when start && duration then calculate_stop
    when stop && duration then calculate_start
    when start && stop then calculate_duration

    when start then calculate_stop_and_duration
    when stop then calculate_start_and_duration
    end
  end

private

  def calculate_stop
    @stop = start + duration
  end

  def calculate_start
    @start = stop - duration
  end

  def calculate_duration
    @duration = stop - start
  end

  def calculate_stop_and_duration
    @stop = @implicit_stop.call
    calculate_duration
  end

  def calculate_start_and_duration
    @start = @implicit_start.call
    calculate_duration
  end
end
