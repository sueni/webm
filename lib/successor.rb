# frozen_string_literal: true

class Successor
  def success
    @success ||= nil
  end

  def success?
    success
  end

  def call
    yield
    self
  end

  def with_real_success
    @success = true
    return true if $opt.dry_run
    yield
  end
end
