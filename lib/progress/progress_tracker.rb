# frozen_string_literal: true

require_relative "progress_indicator"
require_relative "../ui/console"

class ProgressTracker
  def initialize(**args)
    @command = args[:command]
    @duration = args[:duration]
    @framerate = args[:framerate]
    @clean_up = args.fetch(:clean_up, true)
    @progress = ProgressIndicator.new(
      glyphs: " ▁▂▃▄▅▆▇█".chars,
      color: args[:color] || ->_ { _ },
      slots: 17,
      extra: args[:extra] || -> { },
    )
  end

  def call
    Console.with_hidden_cursor do
      IO.popen(@command, :err => [:child, :out]) do |io|
        io.each("\r").with_index do |line, idx|
          /frame=\s*(?<frame>\d+)/ =~ line
          next printf("\n%s\n", Color.err[line]) unless frame
          @progress.draw (frame.to_f / @duration / @framerate).clamp(0, 1)
        end
      end
    end
    Console.clean_line if @clean_up
  end
end
