# frozen_string_literal: true

require_relative "glyph_table"

class ProgressBar
  def initialize(glyphs, slots)
    @slots = slots
    @glyph_table = GlyphTable.new(glyphs, slots).table
  end

  def draw(factor)
    unless (0..1).cover? factor
      raise ArgumentError, "Factor is out of bounds: #{factor}"
    end

    format(
      "%-#{@slots}s",
      @glyph_table.find { |range, _| range.cover? factor }.last
    )
  end
end
