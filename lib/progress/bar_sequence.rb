# frozen_string_literal: true

class BarSequence
  def initialize(glyphs)
    @glyphs_count = glyphs.count
    @iterator = glyphs.cycle
    @keep = +""
    @counter = 1
  end

  def next
    if @counter > @glyphs_count
      @keep << @last_value
      @counter = 1
    end
    @last_value = @iterator.next
    @counter += 1
    @keep + @last_value
  end
end

