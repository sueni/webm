# frozen_string_literal: true

module HumanReadable
  @@table = {1024 => "B",
             1024 ** 2 => "KiB",
             1024 ** 3 => "MiB",
             1024 ** 4 => "GiB",
             Float::INFINITY => "Very Big"}

  refine Integer do
    def to_filesize
      size, measure = @@table.find { |s, _| self < s }
      format("%g %s", self.fdiv(size / 1024).round(2), measure)
    end
  end
end
