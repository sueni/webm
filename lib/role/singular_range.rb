# frozen_string_literal: true

module SingularRange
  refine Range do
    def singular?
      self.begin == 1 and self.end == 1
    end
  end
end
