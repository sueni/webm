# frozen_string_literal: true

module Sanitizeable
  refine String do
    def sanitize
      tr_s " ,/", "__|"
    end

    def desanitize
      tr_s "_", " "
    end
  end
end
