# frozen_string_literal: true

module OnceRunner
  extend self
  @ran = false

  def run_once
    return if @ran
    yield
    @ran = true
  end
end
