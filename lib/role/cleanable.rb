# frozen_string_literal: true

require_relative "naming_templates"

module Cleanable
  refine String do
    def clean
      return self if self.empty?

      delete_prefix("file:").then do |title|
        NamingTemplates.find(title)&.extract or title
      end
    end
  end
end
