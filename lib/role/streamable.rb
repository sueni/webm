# frozen_string_literal: true

module Streamable
  def zero_stream(specifier)
    specifier.include?(":") ? specifier : "0:#{specifier}"
  end

  def subtitle_stream(specifier)
    specifier.start_with?("s:") ? specifier : "s:#{specifier.to_i - 1}"
  end
end
