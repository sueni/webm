# frozen_string_literal: true

require "pathname"

module Colorizeable
  module LsColors
    extend self

    LS_COLORS = ENV['LS_COLORS']

    @@color_codes = {} unless LS_COLORS

    def call(string)
      color = code_for(string)
      return string if not color or not $stdout.tty?
      wrap(string, color)
    end

  private

    def wrap(string, color)
      format("\e[%sm%s\e[0m",color, string)
    end

    def color_codes
      @@color_codes ||= LS_COLORS
        .split(':')
        .select { |c| c.start_with? '*.' }
        .map { |str| str.delete_prefix('*.')
        .split('=') }
        .to_h
        .freeze
    end

    def code_for(string)
      color_codes[Pathname(string).extname[1..]]
    end
  end

  refine String do
    def ls_color
      LsColors.call self
    end
  end

  refine Pathname do
    def ls_color
      LsColors.call self.to_s
    end
  end
end
