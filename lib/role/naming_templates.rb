# frozen_string_literal: true

module NamingTemplates
  extend self

  Template = Struct.new(:source, :pattern, :template, :example) do
    def match(title)
      self.pattern.match(title)
    end

    def extract
      template % NamingTemplates.match
    end
  end

  def list
    [
      Template.new(
        :rarbg_movie,
        /(?<title>[a-z0-9.]+?)\.\(?(?<year>\d{4})\)?(?=\.)/i,
        "%{title}.%{year}",
        "Angry.10.Men.2018.fooo.bar"
      ),

      Template.new(
        :rarbg_series,
        /(?<title>[a-z0-9.]+?)\.(?<season>S\d{2}E\d{2})/i,
        "%{title}.%{season}",
        "Wild.Life.in.1900.S01E01.2018.fooo.bar"
      ),

      Template.new(
        :rarbg_xxx,
        /rarbg(?:\.com)? - (?<studio>[a-z0-9.-]+?)\.\d\d\.\d\d\.\d\d\.(?<title>[a-z0-9.-]+?)\.(?=XXX\.)/i,
        "[%{studio}] - %{title}",
        "RARBG.COM - FooBar.14.09.23.Baz.Quxx.XXX.720p.MP4-KTR"
      ),
    ]
  end

  def find(title)
    list.find do |item|
      item.pattern.match(title) do |match|
        @match = match.named_captures.transform_keys(&:to_sym)
      end
    end
  end

  attr_reader :match
end
