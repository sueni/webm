# frozen_string_literal: true

module Console
  extend self

  def with_hidden_cursor
    hide_cursor
    yield
    self
  ensure
    show_cursor
  end

  def hide_cursor
    print "\e[?25l"
  end

  def show_cursor
    print "\e[?25h"
  end

  def clean_line
    print "\r\e[K"
  end

  def updating_line
    print "\r\e[K", yield
  end
end
