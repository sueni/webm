# frozen_string_literal: true

require "yaml"
require "pathname"

module PositionsStore
  extend self

  STORE = Pathname "/tmp/positions_store.yml"

  def method_missing(method)
    first_item[method]
  end

  def save(item)
    stack = self.stack[0..3]
    stack.prepend(item)
    STORE.write stack.to_yaml
  end

  def first_item
    self.stack.first
  end

  def pop_or_new(path)
    if idx = self.stack.find_index { _1[:file] == path.to_s }
      self.stack.delete_at(idx)
    else
      Hash.new
    end
  end

  def last_delogos
    last_item_delogos = self.stack.find { _1.has_key?(:delogos) }&.fetch(:delogos)
    current_item_delogos = first_item[:delogos]
    if last_item_delogos
      if current_item_delogos != last_item_delogos
        $stderr.puts(Color.warn["Using previous delogo"])
      end
      return last_item_delogos
    end
  end

private

  def stack
    @stack ||= self.yaml || [Hash.new]
  end

  def yaml
    YAML.load(STORE.read) if STORE.file?
  end
end
